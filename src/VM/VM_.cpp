#include <limits>
#include <iostream>

#define INT_INF_P std::numeric_limits<int>::max()
#define INT_INF_M std::numeric_limits<int>::min()
#define INT_R_INF_P isqrt(std::numeric_limits<int>::max())
#define INT_R_INF_M isqrt(std::numeric_limits<int>::min())
#define REAL_INF_P std::numeric_limits<double>::max()
#define REAL_INF_M std::numeric_limits<double>::min()
#define REAL_R_INF_P isqrt(std::numeric_limits<double>::max())
#define REAL_R_INF_M isqrt(std::numeric_limits<double>::min())

int VM_int_add(int left, int right)
{
	try
	{
		int r = left + right;
		if(left > 0 && right > 0 && r <= 0)
		{
			throw("Result out of range : too big");
		}
		if(left < 0 && right < 0 && r >= 0)
		{
			throw("Result out of range : too small");
		}
	)
	catch(string &err)
	{
		callerr("Result out of range : ");	
	}
	return r;
}

int VM_int_sub(int left, int right)
{
	int r = left - right;
	// Pas sur que ca marche.
	if(left > right && right < 0 && r <= 0)
	{
		throw("Result out of range : too big");
	}
	if(left < right && right > 0 && r >= 0)
	{
		throw("Result out of range : too small");
	}
	return r;
}

int VM_int_prod(int left, int right)
{
	int r = left * right;
	try
	{
		if(left > R_INF_P && right > R_INF_P) 	// Advanced calculation of limits.
		{
			throw("Result out of range : too big");
		}
		double s = (double) left * (double) right;
		if(s > INF_P)
		{
			throw("Result out of range : too big");
		}
		if(s < INF_M)
		{
			throw("Result out of range : too small");
		}
	}
	catch()	// Une petit prise de (catch) tête ...
	{
		std:cerr << ...;
	]
	
	return r;
}

int VM_int_division(int num, int div)
{
	try
	{
		if(div == 0)
			throw(1); // "Divide by 0"
	}
	catch(int errno)
	{
		if(errno == 1)	// Dans le cas d'une division par zéro on affecte max_in ou min_int au résultat.
		{
			cerr << "Error : Divide by 0. Result may be incoherent and/or wrong." << endl;
			div = 1;
			if(num >= 0)
				num = INF_P;
			else
				num = INF_M.
		}
	}
	return (int) num / div;
}


// --------------------------------------------------------------------------------------------->
// Lets have fun with double numbers !


int VM_double_add(double left, double right)
{
	try
	{
		double r = left + right;
		if(left > 0 && right > 0 && r <= 0)
		{
			throw("Result out of range : too big");
		}
		if(left < 0 && right < 0 && r >= 0)
		{
			throw("Result out of range : too small");
		}
	)
	catch(string &err)
	{
		cerr << "Result out of range : " << endl;
	}
	return r;
}

int VM_double_sub(double left, double right)
{
	double r = left - right;
	// Pas sur que ca marche.
	if(left > right && right < 0 && r <= 0)
	{
		throw("Result out of range : too big");
	}
	if(left < right && right > 0 && r >= 0)
	{
		throw("Result out of range : too small");
	}
	return r;
}


// LA C'EST CARREMENT DU SUICIDE !
/*
int VM_double_prod(int left, int right)
{
	int r = left * right;
	try
	{
		if(left > R_INF_P && right > R_INF_P) 	// Advanced calculation of limits.
		{
			throw("Result out of range : too big");
		}
		double s = (double) left * (double) right;
		if(s > INF_P)
		{
			throw("Result out of range : too big");
		}
		if(s < INF_M)
		{
			throw("Result out of range : too small");
		}
	}
	catch()	// Une petit prise de (catch) tête ...
	{
		std:cerr << ...;
	]
	
	return r;
}

int VM_double_division(double num, double div)
{
	try
	{
		if(div == 0)
			throw(1); // "Divide by 0"
	}
	catch(int errno)
	{
		if(errno == 1)	// Dans le cas d'une division par zéro on affecte max_in ou min_int au résultat.
		{
			cerr << "Error : Divide by 0. Result may be incoherent and/or wrong." << endl;
			div = 1;
			if(num >= 0)
				num = INF_P;
			else
				num = INF_M.
		}
	}
	return (int) num / div;
}
*/
