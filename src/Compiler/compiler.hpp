#ifndef __COMPILER_HH__
#define __COMPILER_HH__

#include <cstdlib>
#include <iostream>
#include <unistd.h>
#define CREATE_VARS
#include "parser.h"
#include "scanner.h"
#include "info.cpp"

using namespace std;

// D�finition de la class Parser.
class Parser : public CpyrrParser
{
	private:
		CpyrrScanner scanner;
	public:
		Parser();
		virtual void yyerror(char* msg);
		virtual int yylex();
};

// Un constructeur vide.
Parser::Parser()
{
}

// Fonction d'erreur � g�rer.
void Parser::yyerror(char* msg)
{
	cerr << "line " << scanner.line << " : ";

	
	/* << ":" << yylloc.first_column <<  ": "*/
		//<< msg << " : <" 
		cerr << "Wrong usage of \"" << yylloc.text << "\"" <<  endl;

	
	return;
}

// D�finition de la fonction Parser::yylex qui appelle Scanner::yylex.
int Parser::yylex()
{
	yylloc.first_line = scanner.line;
	yylloc.first_column = scanner.column;
	int token = scanner.yylex(&yylval, &yylloc);
	yylloc.last_line = scanner.line;
	yylloc.last_column = scanner.column;
	yylloc.text = (char *) scanner.yytext;
	return token;
}

#endif