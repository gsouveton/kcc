#ifndef __KCC_HELP__
#define __KCC_HELP__

using namespace std;

void KCCHelp(void)
{
	cout << "Usage: kcc [options] file ..." << endl;
	cout << "Options :" << endl;
  	cout << "     -h / --help / -?         Displays this help message" << endl;
	cout << "     -o <file>                Place the output into <file>" << endl;
	cout << "     -S					   No semantic controls - Only for debugging" << endl;
	cout << "	  -v                       Verbose mode" << endl;
	cout << "	  -V                       Displays the current version of KCC" << endl << endl;
	
	return;
}

void KCCAbout(void)
{
	cout << "KCC !" << endl;
	
	return;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                    //
//                                      THIS IS NOT AN EASTER EGG !                                   //
//                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////

void KCCMiau(void)
{
	fprintf(stdout," ___  ____    ______    ______ \n");
	fprintf(stdout,"|_  ||_  _| .' ___  | .' ___  |                  	          KITTY    CPYRR     COMPILER \n");
	fprintf(stdout,"  | |_/ /  / .'   \\_|/ .'   \\_|\n");
	fprintf(stdout,"  |  __'.  | |       | |       \n");
	fprintf(stdout," _| |  \\ \\_\\ `.___.'\\\\ `.___.'\\\n");
	fprintf(stdout,"|____||____|`.____ .' `.____ .'            4B 43 43  2D  4B 69 74 74 79  43 70 79 72 72  43 6F 6D 70 69 6C 65 72 \n");

	fprintf(stdout,"\n");
	fprintf(stdout,"\n");
		  
		  
	fprintf(stdout,"                              .ZMMMMMN.     ..:+=.                                       \n");
	fprintf(stdout,"        ,NMNZ~..             .MMZZZZZMM,.ZMMMMMMMM?.                                     \n");
	fprintf(stdout,"      .+MM+=ZMMMMN......,++I7MMZZZZZZZMMMM~......+M~                                     \n");
	fprintf(stdout,"      ~M8.......~MMMMMMMMMMMMMZZZZZZZZZM8.........MM                                     \n");
	fprintf(stdout,"      MM...................~MZZZZZMMMMZMM8,.......MM.                                    \n");
	fprintf(stdout,"     .MM...................NMZZZZZMZZMM8ZZMMMNMMMMMM.                                    \n");
	fprintf(stdout,"     .MN...................M8ZZZZZMMMMZZZZZZMMZZZZZMM,                                   \n");
	fprintf(stdout,"     .MM...................IMZZZZZZZMZZZZZZZMMZZZZZZMM                                   \n");
	fprintf(stdout,"      MMMM..................NMM8Z8MMM8ZZZZZZMMMMZZZZMM..                                 \n");
	fprintf(stdout,"      ,MM.....................+ZZ?,.?M8ZZZ8MMZMMZZZZMZ,,                                 \n");
	fprintf(stdout,"     .MM..............................+MMMMZZ8ZZZZZMN.MMZ+~,                             \n");
	fprintf(stdout,"     +M7.................................~MZZZZZZMMMM.MMMMMM8I~,                         \n");
	fprintf(stdout,"    .MN...................................:MMMMMMN.,MIMMMMMMMMMN                         \n");
	fprintf(stdout,"    ?M~.............................................MMMMMMMMMMMM                         \n");
	fprintf(stdout,"    NM..............................................MM.?ZMMMMMMM                         \n");
	fprintf(stdout,"    MM...........................................~NMMMMMMMMN8MMM                         \n");
	fprintf(stdout,"..:+IMMZI:........................................?I=MM,......~78                        \n");
	fprintf(stdout,"NN+,:MM,........~MMN.....................MMM+........MM                                  \n");
	fprintf(stdout,"    ,MI........ZMMM.....................MMMZ.......,MM                                   \n");
	fprintf(stdout,"    .MM:,,......MN..........ZDZ:........+MZ........7MMMMMM                               \n");
	fprintf(stdout,"  +MMMMM,..................MIIIM+..................MM                                    \n");
	fprintf(stdout,"  ..  IMZ..................8MDMM................ZIMM,                                    \n");
	fprintf(stdout,"       ,MMMM=...................................,MMMMN                                   \n");
	fprintf(stdout,"     .IMMMMM...................................~MM,..7MM,                                \n");
	fprintf(stdout,"   .NM?.  ,NMM7..............................ZMM+                                        \n");
	fprintf(stdout,"   ...      .,MMMMNI.....................IMMMM,                                          \n");
	fprintf(stdout,"           .:NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM,                                          \n");
	fprintf(stdout,"         .NMM+ZZZMMZZZMM~~,,,,++??,M8ZZZMMZZ~7MMN,                                       \n");
	fprintf(stdout,"       .MMM.ZZ$,MMZZZZZM7~,,,,,,,,MMZZZZZMM.ZZ+.MMN                                      \n");
	fprintf(stdout,"      ~MM+MM~.ZMMZZZZZZZMMD:,,,~MM8ZZZZZZZMM+.ZMM+MM                                     \n");
	fprintf(stdout,"     ,M8...:MMMMZZMMMMZZZZZ8MMM8ZZZZZMMM8ZZMMMM:...8M~                                   \n");
	fprintf(stdout,"    ,MZ......+MZZ8+..~MZZZZZZZZZZZZZM~..M8ZZM7......MM                                   \n");
	fprintf(stdout,"    +M.......MMZZZM~,M8ZZZZZZZZZZZZZMM.~MZZZMM......?M                                   \n");
	fprintf(stdout,"    ,M~.....ZMZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ8M......MM                                   \n");
	fprintf(stdout,"    .NM~....MMZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZMI....+MM                                   \n");
	fprintf(stdout,"      8MMMMMMMZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZMMMMMMM,                                    \n");
	fprintf(stdout,"        ...:M8ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ8M+,,                                       \n");
	fprintf(stdout,"           .M8ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ8M:M8                                       \n");
	fprintf(stdout,"           ,M8ZZZZZZZZZZZZZZZMZZZZZZZZZZZZZZZZM,MN                                       \n");
	fprintf(stdout,"           .MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:M7                                       \n");
	fprintf(stdout,"           ,MI..............7M...............+M:N,                                       \n");
	fprintf(stdout,"           .MD..............7M...............ZM.+                                        \n");
	fprintf(stdout,"            MM..............?M,.............,MZ.                                         \n");
	fprintf(stdout,"            ,MM.............MM7.............MM.,                                         \n");
	fprintf(stdout,"             .NMMMMD?7IIZNMMM7MMMNZII7ZZMMMM7.+                                          \n");
	fprintf(stdout,"                ..IZMNMMZ7,.....+7NMMMMZ+..MM8,                                          \n");
	fprintf(stdout,"                                      ..?MMMN+                                           \n");
	fprintf(stdout,"                                      .:NMMM8                                            \n");
	fprintf(stdout,"                                     ..IMMMM+                                            \n");
	fprintf(stdout,"                                    ..ZMMMM~                                             \n");
	fprintf(stdout,"                                    .,NMMMD.                                             \n");
	fprintf(stdout,"                                    ..ZMMMM~                                             \n");
	fprintf(stdout,"                                    .,NMMMD                                              \n");
	fprintf(stdout,"\n\n");
	return;
}

#endif