%name CpyrrParser
%define LSP_NEEDED
%define ERROR_BODY = 0
%define LEX_BODY = 0

%header{

#include <iostream>
#include <fstream>

#include "../Global/Semantic.hpp"

using namespace std;

// Cr�ation d'un analyseur s�mantique.
#ifdef CREATE_VARS
Semantic program;
// string temps = "";
#endif
#ifndef CREATE_VARS
extern Semantic program;
// extern string temps;
#endif

%}

%union
{
	int i_type;
	char* s_type;
	void* expr_type;	// Tree<Expression> *
	void* ins_type;		// Tree<Instruction> *
	void* type_type;	// Tree<Type> *
	void* dec_type;		// Declaration*
	void* const_type;	// Constante*
	void* var_type;		// Variable*
	void* lvar_type;	// Tree<Variable>*
	void* acc_type;		// Tree<Accessor>*
	
	// TODO
	// %type <expr_type> calcul_entier
}

%token VOID

%token PROG T_EOF

%token IDF
%type <s_type> IDF
%type <s_type> idf

%type <expr_type> expression
%type <ins_type> instruction
%type <s_type> nom_type

%type <ins_type> affectation
%type <ins_type> condition
%type <ins_type> tant_que
%type <ins_type> lecture
%type <ins_type> ecriture

%type <ins_type> liste_instructions
%type <ins_type> suite_liste_inst

%type <s_type> INT
%type <s_type> REAL
%type <s_type> CHAR
%type <s_type> STRING
%type <s_type> BOOL

%type <s_type> type_simple

%type <s_type> T_INT
%type <s_type> T_REAL
%type <s_type> T_CHAR
%type <s_type> T_BOOL
%type <s_type> T_STRING

%type <var_type> variable
%type <lvar_type> suite_liste_variable
%type <lvar_type> liste_variables

%type <acc_type> suitevar

%type <i_type> calcul_entier
%type <const_type> constante_simple

%type <expr_type> un_arg_vect
%type <expr_type> liste_args_vect

%type <expr_type> un_arg_fonction	
%type <expr_type> liste_args_fonction
%type <expr_type> liste_arguments

%type <expr_type> appel
%type <expr_type> calcul
%type <expr_type> comparaison
%type <expr_type> resultat_retourne

%type <expr_type> liste_parametres
%type <expr_type> liste_param
%type <expr_type> un_param





%type <expr_type> EXPRESSION_VIDE

%type <ins_type> VIDE

%type <ins_type> corps

%token OHOOK CHOOK OBRACKET CBRACKET T_BEGIN T_END

%token INT REAL CHAR BOOL STRING

%token T_INT T_REAL T_CHAR T_BOOL T_STRING

%token VARIABLE

%token T_TABLEAU DE

%token T_TYPE

%token T_STRUCT T_ENDSTRUCT

%token WHILE DO READ WRITE

%token IF THEN ELSE

%token OP_NOT OP_NOT_EQUAL

%token EXPRESSION_VIDE

%token FUNCTION RETOURNE PROCEDURE VIDE

%token OP_EQUAL OP_INFEQ OP_SUPEQ OP_INF OP_SUP


%token COLON SEMICOLON COMMA DOT TO

%token OP_ADD OP_DIFF OP_MULT OP_DIV OP_MOD 

%token OP_INCR OP_DECR

%token OP_ADD_AFF OP_MULT_AFF OP_DIV_AFF OP_MOD_AFF OP_DIFF_AFF OP_AFF

%token UNKNOWN_CHAR



%left OP_AFF

%left OP_EQUAL OP_INFEQ OP_SUPEQ OP_INF OP_SUP OP_NOT_EQUAL

%left OP_ADD OP_DIFF

%left OP_MULT OP_DIV OP_MOD


%right DOT


%nonassoc UNARY OP_ADD_AFF OP_MULT_AFF OP_DIV_AFF OP_MOD_AFF OP_DIFF_AFF OP_INCR OP_DECR


%nonassoc OHOOK CHOOK OBRACKET CBRACKET T_BEGIN T_END THEN ELSE COMMA


%%

programme	: PROG corps { program.end(); }
			;

corps		: liste_declarations  SEMICOLON { /*program.dumpDeclaration(); */} liste_instructions
			;

liste_declarations	: declaration
					| liste_declarations SEMICOLON declaration
					;

liste_instructions	: T_BEGIN suite_liste_inst T_END { $$ = $2; }
					;

suite_liste_inst	: instruction SEMICOLON { /* faut cr�er un arbre ici */}
					| suite_liste_inst SEMICOLON instruction SEMICOLON { /* faut cr�er un arbre ici */}
					| { /* retourner insrtuction vide   cad  Tree<Ins>* = NULL */   }
					;
					
declaration	: declaration_type
			| declaration_variable 
			| declaration_procedure
			| declaration_function
			;

declaration_type	: T_TYPE idf { program.newType($2); } COLON suite_declaration_type
					;

suite_declaration_type	: T_STRUCT liste_champs SEMICOLON T_ENDSTRUCT { }
						| T_TABLEAU dimension DE nom_type { program.setType( $4 ); }
						;

dimension	: OHOOK liste_dimensions CHOOK
			;

liste_dimensions	: une_dimension
					| liste_dimensions COMMA une_dimension
					;

une_dimension	: calcul_entier TO calcul_entier { program.addDim($1,$3); }
				;

liste_champs	: un_champ
				| liste_champs SEMICOLON un_champ
				;

un_champ	: idf COLON nom_type { program.addField( $1,  $3); }
			;

nom_type	: type_simple { $$ = $1; }
			| idf		  { sprintf($$,$1,"%s"); }
			;

type_simple	: T_INT       { $$ = "int"; }
			| T_REAL      { $$ = "float"; }
			| T_BOOL      { $$ = "bool"; }
			| T_CHAR      { $$ = "char"; }
			| T_STRING    { $$ = "string"; }
			;

declaration_variable	: VARIABLE IDF COLON nom_type {  program.newVariable($2,$4); }
			;

declaration_procedure	: PROCEDURE idf { program.addArea(); } liste_parametres corps { program.addFunction($2, (Tree<FUN_Args>*)$4, (Tree<Instruction>*)$5); }
						;

declaration_function	: FUNCTION idf { program.addArea(); } liste_parametres RETOURNE nom_type corps { program.addFunction($2, (Tree<FUN_Args>*)$4, (Tree<Instruction>*)$7, $6); }
						;

liste_parametres	:	OBRACKET liste_param CBRACKET { $$ = $2; }
					|	VIDE						  { $$ = NULL }
					;
					
liste_param		: un_param							{ $$ = $1; }
				| liste_param SEMICOLON un_param 	{ $$ = program.addParam((Tree<Expression> *) $1, (Tree<FUN_Args> *) $3); }
				;

un_param	: idf COLON nom_type { $$ = program.addArg($1 ,$3); }
			;

instruction	: affectation					{ $$ = $1; }
			| condition						{ $$ = $1; }
			| tant_que						{ $$ = $1; }
			| VIDE							{ $$ = $1; }
			| RETOURNE resultat_retourne	{ /*$$ = program.functionReturn( (Tree<Expression> *) $2); */}
			| lecture						{ $$ = $1; }
			| ecriture						{ $$ = $1; }
			| expression					{ /* WARNING : STATEMENT HAS NO EFFECT */ 
											  /* + inclure arbre d'expression dans instruction */ }
			;
			
ecriture	: WRITE OBRACKET STRING liste_variables CBRACKET { $$ = program.write( $3 , (Tree<Expression>*) $4); }
			;

lecture		:	READ OBRACKET STRING liste_variables CBRACKET { $$ = program.read( $3 , (Tree<Expression>*) $4); }
			;
			
liste_variables		:	COMMA suite_liste_variable { $$ = $2; }
					| 	{ $$ = program.voidExpr(); }
					;
				
suite_liste_variable : suite_liste_variable COMMA variable { $$ = program.addListExpr( (Tree<Expression> * ) $1, (Expression * ) $3); }
					 |                            variable { $$ = program.listExpr( (Expression * ) $1); }
					 ;
			
resultat_retourne	: expression		{ $$ = $1; }
					| EXPRESSION_VIDE	{ $$ = program.voidExpr(); }
					;

appel	: idf liste_arguments { $$ = program.callFunction($1, (Tree<Expression>*) $2); }
		;

liste_arguments	: OBRACKET liste_args_fonction CBRACKET	{ $$ = $2; }
				| VIDE									{ $$ = program.voidExpr(); }
				;

liste_args_fonction	: un_arg_fonction { $$ = $1; }
					| liste_args_fonction COMMA un_arg_fonction
					;

un_arg_fonction		: expression { $$ = $1; }
					;

liste_args_vect	: un_arg_vect { $$ = $1; }
				| liste_args_vect COMMA un_arg_vect { $$ = program.buildExpr( (Tree<Expression> *) $1, (Tree<Expression> *) $3); }
				;

un_arg_vect		: expression { $$ = $1; }
				;

condition	: IF expression THEN liste_instructions ELSE liste_instructions { If( (Tree<Expression> *) $2 ,
                                                                                  (Tree<Instruction> *) $4 ,
																				  (Tree<Instruction> *) $6 ); }
			| IF expression THEN liste_instructions { If( (Tree<Expression> *) $2 , (Tree<Instruction> *) $4); }
			;

tant_que	: WHILE expression DO liste_instructions { While( (Tree<Expression> *) $2, (Tree<Instruction> *) $4); }
			;

affectation	: variable OP_AFF expression      { $$ = program.affectation ( (Variable *) $1, (Tree<Expression> *) $3 , AFF); }
			| variable OP_ADD_AFF expression  { $$ = program.affectation ( (Variable *) $1, (Tree<Expression> *) $3 , ADD_AFF); }
			| variable OP_DIFF_AFF expression { $$ = program.affectation ( (Variable *) $1, (Tree<Expression> *) $3 , DIFF_AFF); }
			| variable OP_MULT_AFF expression { $$ = program.affectation ( (Variable *) $1, (Tree<Expression> *) $3 , MULT_AFF); }
			| variable OP_DIV_AFF expression  { $$ = program.affectation ( (Variable *) $1, (Tree<Expression> *) $3 , DIV_AFF); }
			| variable OP_MOD_AFF expression  { $$ = program.affectation ( (Variable *) $1, (Tree<Expression> *) $3 , MOD_AFF); }
			| variable OP_INCR                { $$ = program.voidExpr(); /*program.inc    ( (Tree<Expression> *) $1); */}
			| variable OP_DECR                { $$ = program.voidExpr(); /*program.dec    ( (Tree<Expression> *) $1); */}
			| OP_INCR variable                { $$ = program.voidExpr(); /*program.inc    ( (Tree<Expression> *) $2); */}
			| OP_DECR variable                { $$ = program.voidExpr(); /*program.dec    ( (Tree<Expression> *) $2); */}
			;

			
variable	: idf { $$ = program.newVar($1); }
			| idf suitevar { $$ = program.newVar($1, (Tree<Accesseur> *) $2); }
			;

suitevar	: DOT idf { $$ = program.newAcc($2); }
			| DOT idf suitevar { $$ = program.newAcc($2, (Tree<Accesseur> *) $3); }
			| OHOOK liste_args_vect CHOOK { $$ = program.newAcc( (Tree<Expression> *) $2); }
			| OHOOK liste_args_vect CHOOK suitevar { $$ = program.newAcc( (Tree<Expression> *) $2, (Tree<Accesseur> *) $4); }

expression	: OP_NOT  expression %prec UNARY { $$ = $2; }
			| OP_DIFF expression %prec UNARY { $$ = $2; }
			| appel { $$ = $1 ; /* On r�cup�re un pointeur vers l'appel */ }
			| comparaison { $$ = $1; /* La on r�cup�re est Tree<Eprr>* et non un bool ... */ }
			| constante_simple { $$ = $1; }
			| OBRACKET expression CBRACKET { $$ = $2; /*program.priority( (Tree<Expression> *) $2 ); */}
			| variable { $$ = $1; }
			| calcul { $$ = $1; /* Et oui on r�cup�re le pointeur vers l'arbre d'expression ! */ }
			;

comparaison	: expression OP_EQUAL expression     { program.comparaison( (Tree<Expression> *) $1, (Tree<Expression> *) $3, REL_EQUAL);     }
			| expression OP_INFEQ expression     { program.comparaison( (Tree<Expression> *) $1, (Tree<Expression> *) $3, REL_INFEQ);     }
			| expression OP_SUPEQ expression     { program.comparaison( (Tree<Expression> *) $1, (Tree<Expression> *) $3, REL_SUPEQ);     }
			| expression OP_INF expression       { program.comparaison( (Tree<Expression> *) $1, (Tree<Expression> *) $3, REL_INF);       }
			| expression OP_SUP expression       { program.comparaison( (Tree<Expression> *) $1, (Tree<Expression> *) $3, REL_SUP);       }
			| expression OP_NOT_EQUAL expression { program.comparaison( (Tree<Expression> *) $1, (Tree<Expression> *) $3, REL_NOT_EQUAL); }
			;

constante_simple	: INT    { $$ = program.newConst($1,TYPE_INT);    }
					| REAL   { $$ = program.newConst($1,TYPE_REAL);   }
					| CHAR   { $$ = program.newConst($1,TYPE_CHAR);   }
					| STRING { $$ = program.newConst($1,TYPE_STRING); }
					| BOOL   { $$ = program.newConst($1,TYPE_BOOL);   }
					;

calcul		: expression OP_ADD expression  { program.calc ( (Tree<Expression> *) $1, (Tree<Expression> *) $3 , PLUS  ); }
			| expression OP_MULT expression { program.calc ( (Tree<Expression> *) $1, (Tree<Expression> *) $3 , TIME  ); }
			| expression OP_DIV expression  { program.calc ( (Tree<Expression> *) $1, (Tree<Expression> *) $3 , DIV   ); }
			| expression OP_DIFF expression { program.calc ( (Tree<Expression> *) $1, (Tree<Expression> *) $3 , MINUS ); }
			| expression OP_MOD expression  { program.calc ( (Tree<Expression> *) $1, (Tree<Expression> *) $3 , MOD   ); }
			;
			
			
calcul_entier		: calcul_entier OP_ADD calcul_entier  {  $$ =  $1 + $3 ; }
					| calcul_entier OP_MULT calcul_entier {  $$ =  $1 * $3 ; }
					| calcul_entier OP_DIV calcul_entier  {  $$ =  $1 / $3; }
					| calcul_entier OP_DIFF calcul_entier {  $$ =  $1 - $3; }
					| calcul_entier OP_MOD calcul_entier  {  $$ =  $1 % $3; }
					| INT {  $$ = atoi($1); }
					;

idf 				: IDF { string* a = new string($1); $$ = const_cast<char*>((*a).c_str());}

%%