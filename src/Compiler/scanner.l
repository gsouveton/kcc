%name CpyrrScanner

%define LEX_PARAM YY_CpyrrParser_STYPE *val, YY_CpyrrParser_LTYPE *loc
%define MEMBERS public: int line, column;
%define CONSTRUCTOR_INIT : line(1), column(1)


%header{

#include <cstdlib>
#include <iostream>
#include <sstream>
#include "parser.h"


/// YY_CpyrrParser::_STYPE  == int 
%}


entier 		[0-9]+
reel   		(([0-9]*\.[0-9]+)|([0-9]+\.[0-9]*))
idf    		[a-zA-Z_][a-zA-Z0-9_]*
string		\"([^\"]|\\\")*\"
char		'(.|\\.)'
bool		"true"|"false"

%s COMMENTAIRE

%%



<<EOF>>				{ yyterminate(); }

"//"[^\n]*\n			{ line++;  /* commentaire sur une fin de ligne (ou ligne complete) */ }
"/*"					{ BEGIN COMMENTAIRE; /* on entre dans un commentaire */ }
<COMMENTAIRE>"*/"		{ BEGIN 0; /* on sort du commentaire */ }
<COMMENTAIRE>.\n		{ line++; }
<COMMENTAIRE>\n			{ line++; }
<COMMENTAIRE>.*			{ }



{entier}		{  val->s_type = yytext;                            return CpyrrParser::INT; 			}
{reel}			{  val->s_type = yytext;                            return CpyrrParser::REAL;			}
{char}			{  val->s_type = yytext;                            return CpyrrParser::CHAR; 			}
{string}		{  val->s_type = yytext;                            return CpyrrParser::STRING; 		}
{bool}			{  val->s_type = yytext;                            return CpyrrParser::BOOL;           }


"var"			{  													return CpyrrParser::VARIABLE;		}

"int"			{  /*val->s_type = const_cast<char*>("int"); 	*/		return CpyrrParser::T_INT; 			}
"double"		{  /*val->s_type = const_cast<char*>("real"); 	*/	return CpyrrParser::T_REAL; 		}
"float"			{  /*val->s_type = const_cast<char*>("real"); 	*/	return CpyrrParser::T_REAL; 		}
"real"			{  /*val->s_type = const_cast<char*>("real"); 	*/	return CpyrrParser::T_REAL; 		}
"char"			{  /*val->s_type = const_cast<char*>("char"); 	*/	return CpyrrParser::T_CHAR; 		}
"bool"			{  /*val->s_type = const_cast<char*>("bool"); 	*/	return CpyrrParser::T_BOOL; 		}
"string"		{ /* val->s_type = const_cast<char*>("string"); */		return CpyrrParser::T_STRING; 		}

"vect"			{  								return CpyrrParser::T_TABLEAU; 		}
"of"			{  								return CpyrrParser::DE; 			}

"type"			{  								return CpyrrParser::T_TYPE; 		}

"endstruct"		{  								return CpyrrParser::T_ENDSTRUCT; 	}
"struct"		{  								return CpyrrParser::T_STRUCT; 		}


"while" 		{  								return CpyrrParser::WHILE; 			}
"if" 			{  								return CpyrrParser::IF; 			}
"then" 			{  								return CpyrrParser::THEN; 			}
"else" 			{  								return CpyrrParser::ELSE; 			}
"do" 			{  								return CpyrrParser::DO; 			}
"read"			{  								return CpyrrParser::READ; 			}
"write"			{  								return CpyrrParser::WRITE; 			}

"prog"			{  								return CpyrrParser::PROG; 			}
"program"		{  								return CpyrrParser::PROG; 			}

"function"		{  								return CpyrrParser::FUNCTION; 		}
"func"			{  								return CpyrrParser::FUNCTION; 		}
"return"		{  								return CpyrrParser::RETOURNE; 		}
"returns"		{  								return CpyrrParser::RETOURNE; /* la conjugaison c'est important quoi ...*/}
"->"			{  								return CpyrrParser::RETOURNE; /* caml-like */}


"procedure"		{  								return CpyrrParser::PROCEDURE; 		}
"proc"			{  								return CpyrrParser::PROCEDURE; 		}

"(void)"		{  								return CpyrrParser::VIDE; 			}
"()"			{  								return CpyrrParser::VIDE; 			}
"("				{   							return CpyrrParser::OBRACKET; 		}
")"				{  								return CpyrrParser::CBRACKET; 		}
"["				{  								return CpyrrParser::OHOOK; 			}
"]"				{  								return CpyrrParser::CHOOK; 			}
"{"				{  								return CpyrrParser::T_BEGIN; 		}
"}"				{  								return CpyrrParser::T_END; 			}

"=="			{  								return CpyrrParser::OP_EQUAL; 		}
"<="			{  								return CpyrrParser::OP_INFEQ; 		}
">="			{  								return CpyrrParser::OP_SUPEQ; 		}
"=" 			{  								return CpyrrParser::OP_AFF; 		}
"<"				{  								return CpyrrParser::OP_INF; 		}
">"				{  								return CpyrrParser::OP_SUP; 		}

"to"			{  								return CpyrrParser::TO;				}
"."				{  								return CpyrrParser::DOT; 			}
":"				{  								return CpyrrParser::COLON; 			}
";"				{  								return CpyrrParser::SEMICOLON; 		}
","				{  								return CpyrrParser::COMMA; 			}

"+"				{  								return CpyrrParser::OP_ADD; 		}
"++"			{  								return CpyrrParser::OP_INCR; 		}
"+="			{  								return CpyrrParser::OP_ADD_AFF; 	}

"-"				{  								return CpyrrParser::OP_DIFF; 		}
"--"			{  								return CpyrrParser::OP_DECR; 		}
"-="			{  								return CpyrrParser::OP_DIFF_AFF; 	}

"*"				{  								return CpyrrParser::OP_MULT; 		}
"*="			{  								return CpyrrParser::OP_MULT_AFF; 	}

"/"				{  								return CpyrrParser::OP_DIV; 		}
"/="			{  								return CpyrrParser::OP_DIV_AFF; 	}

"%"				{  								return CpyrrParser::OP_MOD; 		}
"%="			{  								return CpyrrParser::OP_MOD_AFF; 	}

"!"				{  								return CpyrrParser::OP_NOT; 		}
"!="			{  								return CpyrrParser::OP_NOT_EQUAL; 	}

{idf}			{  /* J'avoue que visuellement c'est pas super mais �a marche */
				   string yystext(yytext);
				   yystext = yystext.substr(0, yyleng);
				   val->s_type = const_cast<char*>(yystext.c_str()); /* J'aime ce transtypage :) */
				   cout << "idf : " << yystext << endl;
				   return CpyrrParser::IDF;
				}

[ \t]			{ /* Just do nothing */ 											}
\n				{ line++; }

.				{  /* Un caract�re impr�vu */	return CpyrrParser::UNKNOWN_CHAR; 	}

%%

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                    //
//    Lexicals definitions pour KCC (Kitty Cpyrr Compiler) --------------------------------------->   //
//                                                                                                    //
//    Version 0.80 - Coded by Gayvallet J. && Souveton G.                                             //
//                                                                                                    //
//    Please notice that YOU ARE the kitty ! Thanks reading all these uninteressting lines of code.   //
//    RIP, may the Schroedinger's force be with you forever and never. Remember you're the kitty !    //
//                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////