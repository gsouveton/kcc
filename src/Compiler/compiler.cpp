////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                    //
//    Main file for KCC (Kitty Cpyrr Compiler) including main() function  ------------------------>   //
//                                                                                                    //
//    Version 0.80 - Coded by Gayvallet J. && Souveton G.                                             //
//                                                                                                    //
//    Please DO NOT read the commentaries in lexical/grammatical files.                               //
//    You could be shocked and not be shocked. Remember you're the kitty !                            //
//                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "compiler.hpp"

int main(int argc, char ** argv)
{
	Parser parser;
	
	char c;
	string outputFileName = "a.out";
	
	extern char *optarg;
    extern int optind, optopt;

    while ((c = getopt(argc, argv, ":abf:o:")) != -1)
	{
		switch(c)
		{
			case 'o':
				cerr << optarg;
				outputFileName = optarg;
				case ':':       /* -f or -o without operand */
					cerr << "Please specify an output file" << endl;
				break;
			break;
        case 'c':  break;
        /*case '?':
		case 'h':
			KCCHelp();
			return EXIT_SUCCESS;
		break;
		case 'a':
			KCCAbout();
			return EXIT_SUCCESS;
		break;
		case 'M':
			KCCMiau();
			return EXIT_SUCCESS;
		break;*/
    }
    }


	FILE* fin;
	__FILE* output; /// On n'est pas cens� manipuler ce type de variables :) ? Je vois pas le probl�me :D
	
	// J'avoue, il y a une limite � la mochet� ...
	// Mais au moins �a marche !
	fin = freopen(argv[1],"r",stdin);
	
	// La on va cr�er une r�f�rence sur le fichier de sortie.
	
	if(outputFileName == "")
		output = stdout;
	else
		output = fopen(outputFileName.c_str(),"r");
		
		

	extern Semantic program;

	int a = parser.yyparse();

	if(a)
	{
		cerr << "---------------------------------------------" << endl;
		cerr << "-> Error " << a << ": " << outputFileName << " hasn't been build !" << endl;
		cerr << "---------------------------------------------" << endl << "Maybe you're a kitty !" << endl;
		cerr << "Please remember you're the kitty and you're not the kitty !" << endl;
		cerr << "Please remember that the Kitty's in the box and in Uranus." << endl;
		cerr << "You should check it out !" << endl;
	}
	else
	{
		cerr << "---------------------------------------------" << endl;
		cerr << "-> 01.cml has been built !" << endl;
		cerr << "   You gonna go far kid !" << endl;
		cerr << "---------------------------------------------" << endl;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                    //
//                         To be AND not to be a kitty, that's the answer !                           //
//                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////