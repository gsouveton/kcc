#include "Type.hpp"


int main(void)
{
	Type* t1 = new Type(TYPE_INT);
	Type* t2 = new Type(t1);
	t2->addDim(0, 5);
	Type* t3 = new Type();
	t3->addField("plop", t2);
	t3->addField("plop", t1);
	t1->draw();
	t2->draw();
	t3->draw();
	delete t1;
	delete t2;
	delete t3;
	return 0;
}
