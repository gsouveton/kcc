#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include "Global.hpp"
#include "Tree.hpp"
#include "Declaration.hpp"
#include "Couple.hpp"


class OpExpr
{

public:
void exportToXML(ostream &) const;
};

class Accesseur
{
	
	
	public:
	
	accesseur a;
	Couple c;
	Tree<Expression> * t;

	void exportToXML(ostream &) const;
};

class Variable
{
	private:
		Declaration* var;
		Type* type;
		Tree<Accesseur>* acc;

	public:
		Variable(Declaration * d);
		Variable(Declaration *, Type *, Tree<Accesseur> *);
		Type* getType();
	
	void exportToXML(ostream &) const;
};

class Calc
{
	protected:
		Tree<Expression> * result;
		Tree<Expression> * left;
		Tree<Expression> * right;
		operand o;
	
	public:
		Calc(Tree<Expression> * l, Tree<Expression> * r,operand op);
		void exportToXML(ostream &) const;
		
};

class Comparaison
{
	protected:
		Tree<Expression> * left;
		Tree<Expression> * right;
		rel rela;

	public:
		Comparaison(Tree<Expression> * l, Tree<Expression> * r,rel op);
		void exportToXML(ostream &) const;		
};

// La classe qui g�re les appels de fonction
class Call
{
	private:
		Function* func;				// Un appel est compos� d'une fonction
		Tree<Expression> * args;	// Et d'une suite d'arguaments
		
	public:
		Call(Function * , Tree<Expression> * );
		void exportToXML(ostream &) const;
};



class Constante
{
	private:
		basetype type;
		string value;

	public:
		Constante(basetype b, string val)
		{
			type = b;
			value = val;	
		}

		void exportToXML(ostream &) const;
};



class Expression
{
	// Une expression est forc�mment typ�e.
	private:
		Type* type;		// Le type de retour de l'expression.
		TypeExpr e;		// De quel type d'expression il s'agit (calcul, variable) 
	// 

	bool isVariable;        Variable    * var;
	bool isConst;           Constante   * constante;
	bool isCall;            Call        * call;
	bool isCompare;         Comparaison * comp;
	bool isCalc;            Calc        * calc;
	bool isOpExpr;          OpExpr      * oexpr;
	
	//
	public:
		Expression( TypeExpr );
		Expression( TypeExpr , Type* t);
		Expression( TypeExpr , basetype , char* );
		Expression( TypeExpr, Type*, Tree<Expression> *  , Tree<Expression> *   , operand); // Pour les calcs
		Expression( TypeExpr, Tree<Expression> *  , Tree<Expression> *   , rel); // Pour les comps
		Expression( TypeExpr, Function * , Tree<Expression> * );
		
		void Not();
		Type* getType();
		

	// Fonction d'export
		void exportToXML(ostream &) const;
	
};

#endif