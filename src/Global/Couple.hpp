#ifndef COUPLE_HPP
#define COUPLE_HPP
#include "Global.hpp"


class Couple
{
	public:
		int x;
		int y;
	public:
		Couple(void);
		Couple(int, int);
		bool operator==(const Couple&) const;
		void operator=(const Couple&);
		friend ostream & operator<<(ostream &, const Couple &);
};

#endif
