#include "Area.hpp"

//class Area
//{
//	private:
//		int id;
//		string content;
//	public:
Area::Area(int n)
{
	this->id = n;
	this->content = new Tree<Instruction>;
}

Area::~Area()
{
	content->DeleteBranch();
}
//	public:
int Area::getID()const
{
	return this->id;
}
Tree<Instruction> * Area::getContent(void)const
{
	return this->content;
}

void Area::exportToXML(ostream & out)const
{
	out << "<area id=\"" << id << "\">" << endl;
	if (content != NULL) content->exportToXML(out);
	out << "</area>" << endl;
}

//};

//class Area_Map
//{
//	private:
//		Tree<Area> * map;
//		int id;
//		Tree<Area> * position;
//	public:
Area_Map::Area_Map()
{
	this->id = 0;
	this->map = new Tree<Area>(new Area(0));
	this->position = this->map;
}
Area_Map::~Area_Map()
{
	this->map->DeleteBranch();
}
//	public:
void Area_Map::addArea()
{
	Tree<Area> * next = new Tree<Area>(new Area(++id));
	this->position->addSon(next);
	this->position = next;
}
void Area_Map::stepBack()
{
	if (this->position->getUp() != NULL) this->position = this->position->getUp();
}

Tree<Area> * __getArea(int n, Tree<Area> * T) //hm, de la prog récursive ... comme ça me manquait sérieux :D
{
	if ( T == NULL ) return NULL;
	if ( T->getData()->getID() == n ) return T;
	Tree<Area> * tmp = __getArea(n, T->getRight());
	if ( tmp != NULL ) return tmp;
	return __getArea(n, T->getDown());
}

Tree<Area> * Area_Map::getArea(int n)const
{
	return __getArea(n, this->map);
}
Tree<Area> * Area_Map::getPosition(void)const
{
	return this->position;
}
//};

/*
int main(void)
{
	Area_Map * map = new Area_Map();
	map->addArea();
	map->addArea();
	map->stepBack();
	map->addArea();
	for ( int i = 0 ; i < 10 ; cout << map->getArea(i++) << endl ) {} ;
	delete map;
	return 0;
}
*/