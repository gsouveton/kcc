#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP
#include "Global.hpp"
#include "Tree.hpp"
#include "Declaration.hpp"
#include "Expression.hpp"

class While;
class If;
class Write;
class Read;
class Affectation;
class Variable;

class While
{
	public:
		Tree<Instruction> * instr;
		Tree<Expression> * cond;
		
	While(Tree<Expression> * c, Tree<Instruction> * i);
	void exportToXML(ostream & out) const;

};

class If
{
	public:
		Tree<Expression> * cond;
		Tree<Instruction> * instr;
		Tree<Instruction> * ninstr;
	
	If(Tree<Expression> * c, Tree<Instruction> * i);
	If(Tree<Expression> * c, Tree<Instruction> * i, Tree<Instruction> * ni);
	void exportToXML(ostream & out) const;

	
	
};

class Write
{
	private:
		string texte;
		Tree<Expression> * vars;
	public:
		Write(string,Tree<Expression> *);
		void exportToXML(ostream &) const;
};

class Read
{
	private:
		string texte;
		Tree<Expression> * vars;
	public:
		Read(string,Tree<Expression> *);
		void exportToXML(ostream &) const;
};



// La classe qui g�re les affectations
class Affectation
{
	private:
		Variable *         variable;        // Une variable contient une declaration + un arbre d'accesseur.
		Tree<Expression> * value;	        // Ce qu'on va affecter.
		opaff              op_affectation;  // Type d'affectation.

	public:
	
		Affectation(Variable* , Tree<Expression> * , opaff opt);
		void exportToXML(ostream & out) const;
};









class Instruction
{
	protected:
	
	
	public:
	
	// Type de l'instruction
	TypeIns ins; // INS_IF, INS_WHILE , INS_WRITE , INS_READ , INS_AFF, EXPR
	
	If *				IIf;
	While * 			IWhile;
	Write *				IWrite;
	Read *				IRead;
	Affectation *		IAff;
	Tree<Expression> * 	IExpr;

	
	Instruction( TypeIns );
	Instruction( TypeIns , Tree<Expression> * , Tree<Instruction> *  );	// If-Then
	Instruction( TypeIns , Tree<Expression> * , Tree<Instruction> * , Tree<Instruction> * );	// If-Then-Else

	Instruction( TypeIns , Variable * , Tree<Expression> * , opaff );	// Affectation
	Instruction( TypeIns , string , Tree<Expression> * );				// Write

	void exportToXML(ostream & out) const;

};

#endif