#include "Couple.hpp"

Couple::Couple(void)
{
	this->x = 0;
	this->y = 0;
}

Couple::Couple(int x, int y)
{
	this->x = x;
	this->y = y;
}

bool Couple::operator==(const Couple& c) const
{
	return this->x == c.x && this->y == c.y;
}

void Couple::operator=(const Couple& c)
{
	this->x = c.x;
	this->y = c.y;
}

ostream & operator<<(ostream & out , const Couple& c)
{
	out << "(" << c.x << "," << c.y << ")";
	return out;
}
