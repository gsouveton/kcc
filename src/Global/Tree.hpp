#ifndef TREE_HPP
#define TREE_HPP
#include "Global.hpp"

//La template a été épurée, pas mal de code était inutile|inutilisé et pas du tout template-friendly
template <class T>
class Tree
{
	protected:
		Tree* up;
		Tree* right;
		Tree* left;
		Tree* down;
		T* data;

	// Methods definition
	public:
		//Constructors
		Tree(void); // pour construire une tete vide
		Tree(T* data);
		//Destructors
		~Tree();
	public:
		void addSon(Tree* s); //ajout du fils
		void addSon(T * d);
		void addBrother(Tree* b); //ajout du frere droit
		void addBrother(T * d);
		void addBrotherhood(Tree * bh);

	public:
		//getters, pas besoin de faire de setters public
		Tree* getDown(void) const;
		Tree* getLeft(void) const;
		Tree* getRight(void) const;
		Tree* getUp(void) const;

		Tree* getRoot(void) const;
		T* getData(void) const;
		int cardinal(void)const;
		
		void exportToXML(ostream & out);

	protected:
		void setUp(Tree* u);
		void setRight(Tree* r);
		void setLeft(Tree* l);
		void setDown(Tree* d);

	public:
		void DeleteBranch(void); // fonction de destruction de la branche courante
};
#include "Tree.cxx"
#endif
