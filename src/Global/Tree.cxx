#ifndef TREE_CXX
#define TREE_CXX

// Constructeurs et destructeurs

template <class T> Tree<T>::~Tree()
{
	delete this->data;
}

template <class T> Tree<T>::Tree(void)
{

}
template <class T> Tree<T>::Tree(T* data)
{
	this->up=NULL;
	this->left=NULL;
	this->right=NULL;
	this->down=NULL;
	this->data= data;
}

//methodes
template <class T> void Tree<T>::DeleteBranch(void)
{
	if ( this->down != NULL ) { this->down->DeleteBranch();} //on delete les fils
	if ( this->right != NULL ) { this->right->DeleteBranch();} // on delete les freres droits
	if ( this->up != NULL ) { this->up->down = NULL;} // on indique au père que le fils n'existe plus (ouais, je sais, c'est 'achement triste)
	if ( this->left != NULL ) { this->left->right = NULL;} // on indique au frere gauche que le frere droit n'existe plus (ouais, je sais, c'est 'achement triste aussi)
	delete this;
}

template <class T> Tree<T>* Tree<T>::getDown(void) const
{
	return this->down;
}
template <class T> Tree<T>*Tree<T>::getLeft(void) const
{
	return this->left;
}
template <class T> Tree<T>* Tree<T>::getRight(void) const
{
	return this->right;
}
template <class T> Tree<T>* Tree<T>::getUp(void) const
{
	return this->up;
}
template <class T> Tree<T>* Tree<T>::getRoot(void) const
{
	if (this->up == NULL) { return this;}
	else { return this->up->getRoot();}
}

template <class T> T* Tree<T>::getData(void) const
{
	return data;
}

template <class T> void Tree<T>::setLeft(Tree* l)
{
	this->left = l;
}

template <class T> void Tree<T>::setUp(Tree* u)
{
	this->up = u;
	if ( this->right != NULL ) this->right->setUp(u) ;
}

template <class T> void Tree<T>::addSon(Tree* s)
{
	if ( this->down == NULL )
	{
		this->down = s;
		s->setUp(this);
	}
	else
	{
		this->down->addBrother(s);
	}
}
template <class T> void Tree<T>::addSon(T* d)
{
	return this->addSon(new Tree<T>(d));
}

template <class T> void Tree<T>::addBrother(Tree* b)//ajout du frere droit
{
	if (this->right != NULL) this->right->addBrother(b);
	else { this->right = b; b->setLeft(this); b->setUp(this->up); }
}
template <class T> void Tree<T>::addBrother(T* d)
{
	return this->addBrother(new Tree<T>(d));
}

template <class T> void Tree<T>::exportToXML(ostream & out)
{
	if ( this->data != NULL ) this->data->exportToXML(out);
	if ( this->down != NULL )this->down->exportToXML(out);
	if ( this->right != NULL ) this->right->exportToXML(out);
}

template <class T> void Tree<T>::addBrotherhood(Tree * bh) // bh shoud have no parents
{
	if ( bh != NULL )
	{
		this->addBrother(bh);  
		this->addBrotherhood(bh->getRight());
	}	
}

template <class T> int Tree<T>::cardinal(void)const
{
	if ( right == NULL ) return 0;
	else return 1 + right->cardinal();
}
#endif
