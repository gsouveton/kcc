#include "Type.hpp"

TYPE_Struct::TYPE_Struct(Couple s, Type* t)
{
	this->field_name = s;
	this->field_type = t;
}

Couple TYPE_Struct::getFieldName(void)const
{
	return this->field_name;
}

Type* TYPE_Struct::getFieldType(void)const
{
	return this->field_type;
}

void TYPE_Struct::exportToXML(ostream & out)const
{
	out << "<field" << " id = \"" << field_name << "\" " << "target = \"" << field_type << "\"" << "/>" << endl ;
}

TYPE_Vect::TYPE_Vect(int o, int w)
{
	this->offset = o;
	this->width = w;

}

int TYPE_Vect::getWidth(void)const
{
	return this->width;
}

int TYPE_Vect::getOffset(void)const
{
	return this->offset;
}

bool TYPE_Vect::isInRange(int i)const
{
	return (i >= this->offset && i-this->offset < width);
}

void TYPE_Vect::exportToXML(ostream & out, int i)const
{
	out << "<dim dim_nbr=\"" << i << "\"" << " offset = \"" << offset << "\" " << "width = \"" << width << "\"" << "/>" << endl ;
}

Type::Type(basetype b)
{
	this->isbasetype = true;
	this->isstruct = false;
	this->isvect = false;
	this->T = b;
}

Type::Type(void)
{
	this->isbasetype = false;
	this->isstruct = false;
	this->isvect = false;
	this->TTS = new Tree<TYPE_Struct>();
	this->TTV = new Tree<TYPE_Vect>();
	this->dim = 0;
}

void Type::setVect(Type* t)
{
	this->isvect = true;
	this->type = t;
}
void Type::setStruct(void)
{
	this->isstruct = true;
}

bool Type::isBaseType(void)
{
	return this->isbasetype;
}

basetype Type::baseType(void)
{
	return this->T;
}

/*Type::Type(Type * T)
{
	this->isbasetype = false;
	this->isstruct = false;
	this->isvect = true;
	this->TTV = new Tree<TYPE_Vect>();
	this->type = T;
}*/

Type::~Type(void)
{
	this->TTS->DeleteBranch();
	this->TTV->DeleteBranch();
}

//int Type::getId(void) const
//{
//	return this->id;
//}

void Type::addField(Couple s, Type* T)
{
	this->TTS->addSon(new Tree<TYPE_Struct>(new TYPE_Struct(s, T)));
}

void Type::addDim(int a, int b)
{
	this->dim++;
	this->TTV->addSon(new Tree<TYPE_Vect>(new TYPE_Vect(a, b-a+1)));
}

Type* Type::getFieldType(Couple s) const
{
	Tree<TYPE_Struct> * ptr = this->TTS->getDown();
	Type * out = NULL;
	while ( out == NULL and ptr != NULL )
	{
		if ( ptr->getData()->getFieldName() == s )
		{
			out = ptr->getData()->getFieldType();
		}
		else
		{
			ptr = ptr->getRight();
		}
	}
	return out;
}
bool Type::isInRange(int d, int e) const
{
	if ( d <= dim )
	{
		Tree<TYPE_Vect> * ptr = this->TTV->getDown();
		for ( int i = 0 ; i < dim - d + 1 ; i++ )
		{
			ptr = ptr->getRight();
		}
		return ptr->getData()->isInRange(e);
	}
	else
	{
		return false;
	}
}

void Type::exportToXML(ostream & out) const
{
	if ( isbasetype ) out << "<type id=\"" << this << "\">" << endl << "<bastype id=\"" << T << "\"/>" << endl << "</type>" << endl;
	if ( isvect )
	{
		out << "<type id=\"" << this << "\">" << endl << "<vect ndim=\"" << dim << "\"" << " target=\"" << type << "\">"<< endl;
		Tree<TYPE_Vect> * ptr = this->TTV->getDown();
		for ( int i = 0 ; i < dim ; i++)
		{
			ptr->getData()->exportToXML(out, dim - i - 1);
			ptr = ptr->getRight();
		}
		out << "</vect>" << endl <<"</type>"<< endl;
	}
	if ( isstruct )
	{
		out << "<type id=\"" << this << "\">" << endl;
		Tree<TYPE_Struct> * ptr = this->TTS->getDown();
		while ( ptr != NULL)
		{
			ptr->getData()->exportToXML(out);
			ptr = ptr->getRight();
		}
		out << "</type>"<< endl;
	}
}

int Type::getDim(void)const
{
return this->dim;
}

Type* Type::getType(void)const
{
	return this->type;
}
/*
int main(void)
{
	Type * INT = new Type(TYPE_INT);
	INT->exportToXML(cout);
	Type * VECT_INT = new Type(INT);
	VECT_INT->addDim(0,10);
	VECT_INT->addDim(0,10);
	VECT_INT->addDim(0,10);
	VECT_INT->exportToXML(cout);
	Type * STRUCT_INT = new Type();
	STRUCT_INT->addField(Couple(), INT);
	STRUCT_INT->exportToXML(cout);
	delete STRUCT_INT;
	delete VECT_INT;
	delete INT;
	return 0;
}
*/