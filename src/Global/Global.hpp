#ifndef GLOBAL_HPP
#define GLOBAL_HPP
#include <iostream>
#include <string>
#include <cstdlib>


using namespace std;

#define elseif else if 

typedef enum { VECT = 0, STRUCT } accesseur;
typedef enum { TYPE_BOOL = 0, TYPE_INT, TYPE_REAL, TYPE_CHAR, TYPE_STRING, ENDBASETYPE } basetype;
typedef enum { DEC_VAR = 0, DEC_TYPE, DEC_FUN } basedec;
typedef enum { PLUS = 0, MINUS, TIME, DIV, MOD } operand;
typedef enum { REL_EQUAL = 0, REL_INFEQ, REL_SUPEQ, REL_INF, REL_SUP, REL_NOT_EQUAL } rel;
typedef enum { AFF = 0, ADD_AFF, DIFF_AFF, MULT_AFF, DIV_AFF, MOD_AFF } opaff;
typedef enum { VARIABLE = 0, CALC, CALL, COMP, CONST, OP_EXPR } TypeExpr;
typedef enum { INS_IF = 0, INS_WHILE , INS_CALL , INS_WRITE , INS_READ , INS_AFF, EXPR } TypeIns;
typedef enum { PREFIX = 0 , POSTFIX , INFIX } PosExpr;
typedef enum { INC = 0 , DEC , NOT , DIFF } UnaryOp;

class Instruction;
class Expression;
class Area;
class Declaration;
class Function;
class Type;

#endif