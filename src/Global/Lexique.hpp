#ifndef LEXIQUE_HPP
#define LEXIQUE_HPP
#include "Global.hpp"
#include "Couple.hpp"
#include "Declaration.hpp"

class Word
{
	protected:
		string word;
		Tree<Declaration> * meaning;
		int l;
		int id;

	public:
		Word(string, int);
		~Word(void);

	public:
		int getID(void)const;
		string getWord(void)const;
		int getLength(void)const;
		Tree<Declaration> * getMeaning(void)const;
		void addMeaning(Declaration*);
		void exportToXML(ostream & out);
};

class Lex_table
{
	protected:
		Tree<Word> * table;
		int id;

	public:
		Lex_table();
		~Lex_table();

	public:
		Couple put(string);

		string get(Couple)const;
		string get(int, int)const;
		Couple get(string)const;

		Tree<Declaration> * meaning(string)const;
		Tree<Declaration> * meaning(int, int)const;
		Tree<Declaration> * meaning(Couple)const;

		void addMeaning(Couple, Declaration *);
		void ToXML(ostream & out);
	protected:
		int hash(string)const;
		Couple _put(string, int); 
		Couple _get(string, int)const;

		Word * _getW(string)const;
		Word * _getW(int, int)const;
		Word * _getW(Couple)const;
		
};

#endif
