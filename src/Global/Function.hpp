#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include "Global.hpp"
#include "Type.hpp"
#include "Area.hpp"

class FUN_Args
{
	protected:
		Couple lex;
		Type* type;
	public:
		FUN_Args(Couple, Type*);
	public:
		Couple getLex()const;
		Type* getType()const;
		void exportToXML(ostream & out)const;
};


class Function
{
	protected:
		Tree<FUN_Args> * args;
		Tree<Area> * position;
		Type * type;
		int nargs;

	public:
		Function(Tree<Area>*, Tree<FUN_Args>*, Type*);
		~Function();

	public:
	Type * getArgType(Couple)const;
	Type * getArgType(int)const;
	Type * getReturn()const;
	int getNArgs()const;
	void exportToXML(ostream & out);
	Tree<Area> * getPos(void)const;
};

#endif