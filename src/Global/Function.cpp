#include "Function.hpp"

FUN_Args::FUN_Args(Couple c, Type* t)
{
	this->lex = c;
	this->type = t;
}

Couple FUN_Args::getLex()const
{
	return this->lex;
}
Type* FUN_Args::getType()const
{
	return this->type;
}
void FUN_Args::exportToXML(ostream & out)const
{
	out << " lex =\"" << lex << "\" type=\"" << type << "\"";
}


Function::Function(Tree<Area>* p, Tree<FUN_Args>* args , Type* type)
{
	this->nargs = 0;
	this->type = type;
	this->args = args;
	this->position = p;
}
Function::~Function()
{
	this->args->DeleteBranch();
}
Type * Function::getArgType(Couple c)const
{
	Tree<FUN_Args> * ptr = this->args->getDown();
	while ( ptr != NULL )
	{
		if (ptr->getData()->getLex() == c) return ptr->getData()->getType();
		ptr = ptr->getRight();
	}
	return NULL;
}
Type * Function::getArgType(int i)const
{
	if (i > nargs) return NULL;
	Tree<FUN_Args> * ptr = this->args;
	while ( ptr != NULL and i > 0)
	{
		i--;
		ptr = ptr->getRight();
	}
	if ( i > 0 ) return NULL;
	else return ptr->getData()->getType();
}
Type * Function::getReturn()const
{
	return this->type;
}

int Function::getNArgs()const
{
	return this->nargs;
}

Tree<Area> * Function::getPos(void)const
{
	return this->position;
}

void Function::exportToXML(ostream & out)
{
	out << "<function id=\"" << this << "\">" << endl;
	Tree<FUN_Args> * ptr = args;
	for ( int i = 1 ; i <= nargs ; i++ )
	{
		out << "<arg num=\"" << i << "\"";
		ptr->getData()->exportToXML(out);
		out << "/>" << endl;
		ptr = ptr->getRight();
	}
	out << "</function>" << endl;
}
//};
