#include "Instruction.hpp"


If::If(Tree<Expression> * c, Tree<Instruction> * i)
{
	cond = c;
	instr = i;
	ninstr = NULL;
}

If::If(Tree<Expression> * c, Tree<Instruction> * i, Tree<Instruction> * ni)
{
	cond = c;
	instr = i;
	ninstr = ni;
}

void If::exportToXML(ostream & out) const
{
	out << "<condition>" << endl << "<if id=\"pointeur sur expression\" >" << endl << "<then>" << endl;
	instr->exportToXML(out);
	out << "</then><else>";
	ninstr->exportToXML(out);
	out << "</else>" << endl << "</if>" << endl << "</condition>" << endl;
}
	
While::While(Tree<Expression> * c, Tree<Instruction> * i)
{
	cond = c;
	instr = i;
}

void While::exportToXML(ostream & out) const
{
	out << "<loop>" << endl << "<while id=\"pointeur sur expression\" >" << endl << "<then>" << endl;
	instr->exportToXML(out);
	out << "</then><else>";
	out << "</loop>" << endl;
}

Write::Write(string s, Tree<Expression> * args)
{
	texte = s;
	vars = args;
}

void Write::exportToXML(ostream & out) const
{
	out << "<write>" << endl;
	out << "<text value=\"" << texte << "\" />" << endl;
	out << "<args>" << endl;
	vars->exportToXML(out);
	out << "</args>" << endl;
	out << "</write>" << endl;
}

Read::Read(string s, Tree<Expression> * args)
{
	texte = s;
	vars = args;
}

void Read::exportToXML(ostream & out) const
{
	out << "<scan>" << endl;
	out << "<text value=\"" << "\" />" << endl;
	out << "<args>" << endl;
	vars->exportToXML(out);
	out << "</args>" << endl;
	out << "</scan>" << endl;
}
	
Affectation::Affectation(Variable* var, Tree<Expression> * val, opaff opt)
{
	variable       = var;
	value          = val;
	op_affectation = opt;
};

void Affectation::exportToXML(ostream & out) const
{
	out << "<affectation op=\"" << op_affectation << "\">" << endl;
	variable->exportToXML(out);
	value->exportToXML(out);
	out << "</affectation>" << endl;
}


Instruction::Instruction( TypeIns ti , Tree<Expression> * c, Tree<Instruction> * i )
{
	ins    =  ti;
	IIf    =  NULL; IWhile =  NULL;	IWrite =  NULL;
	IRead  =  NULL;	IAff   =  NULL;	IExpr  =  NULL;
	if( ti == INS_IF)
		this->IIf = new If( c , i );
}

Instruction::Instruction( TypeIns ti , Tree<Expression> * c, Tree<Instruction> * i, Tree<Instruction> * ni )
{
	ins    =  ti;
	IIf    =  NULL; IWhile =  NULL;	IWrite =  NULL;
	IRead  =  NULL;	IAff   =  NULL;	IExpr  =  NULL;
	if( ti == INS_IF)
		this->IIf = new If( c , i , ni );
}

Instruction::Instruction( TypeIns ti , Variable * v , Tree<Expression> * te , opaff o )
{
	ins    =  ti;
	IIf    =  NULL; IWhile =  NULL;	IWrite =  NULL;
	IRead  =  NULL;	IAff   =  NULL;	IExpr  =  NULL;
	if( ti == INS_AFF)
		this->IAff = new Affectation( v ,te , o );
}

Instruction::Instruction( TypeIns ti , string texte , Tree<Expression> * args )
{
	ins    =  ti;
	IIf    =  NULL; IWhile =  NULL;	IWrite =  NULL;
	IRead  =  NULL;	IAff   =  NULL;	IExpr  =  NULL;
	if( ti == INS_WRITE )
		this->IWrite = new Write(texte,args);
	if( ti == INS_READ )
		this->IRead = new Read(texte,args);
}
void Instruction::exportToXML(ostream & out) const
{
		if(ins == INS_WHILE)       IWhile->exportToXML(out);
		else if(ins == INS_IF)     IIf->exportToXML(out);
		else if(ins == INS_WRITE)  IWrite->exportToXML(out);
		else if(ins == INS_READ)   IRead->exportToXML(out);
		else if(ins == INS_AFF)    IAff->exportToXML(out);
		else if(ins == EXPR)       IExpr->exportToXML(out);
}