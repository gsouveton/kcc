#ifndef TYPE_HPP
#define TYPE_HPP

#include "Global.hpp"
#include "Couple.hpp"
#include "Tree.hpp"

class Type;

class TYPE_Struct
{
	private:
	Couple field_name;
	Type* field_type;
	public:
	TYPE_Struct(Couple, Type*);
	Couple getFieldName(void)const;
	Type* getFieldType(void)const;
	void exportToXML(ostream &)const;
};

class TYPE_Vect
{
	private:
	int width;
	int offset;
	public:
	TYPE_Vect(int, int);
	int getWidth(void)const;
	int getOffset(void)const;
	bool isInRange(int)const;
	void exportToXML(ostream &, int)const;
};


class Type
{
//	static Type* current;
//	static int latest_id;

	private:
//	int id;

	bool isbasetype;
	basetype T;
	
	bool isstruct;
	Tree<TYPE_Struct> * TTS;

	bool isvect;
	int dim;
	Type* type;
	Tree<TYPE_Vect> * TTV;

	public:
	Type(basetype); //Déclaration d'un type de base
	Type(void);
//	Type(Type*); //déclaration d'un vect, les arguments sont la dimension et le type des données
	~Type(void);

	public: //setters
	void addField(Couple, Type*);
	void addDim(int, int);
	void setVect(Type*);
	void setStruct(void);

	public: //getters
	Type* getFieldType(Couple) const;
	Type* getType(void)const;
	bool isInRange(int, int) const; //dimension, element
//	int getId(void) const;
	void exportToXML(ostream & out) const;
	int getDim(void)const;
	bool isBaseType(void);
	basetype baseType(void);
};

#endif









