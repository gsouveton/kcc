#include "Declaration.hpp"

//class Declaration
//{
//	private:
//		basedec cas;
//		bool v_init;
//		Tree<Area> * position;
//
//		Type* type;
//		Function* function;
//
//	public:
Declaration::Declaration(Type * t , Tree<Area> * p,bool b)
{
	this->cas = DEC_VAR;
	this->v_init = b;
	this->position = p;
	this->type = t;
	this->function = NULL;
}
Declaration::Declaration(Type * t, Tree<Area> * p)
{
	this->cas = DEC_TYPE;
	this->v_init = true;
	this->position = p;
	this->type = t;
	this->function = NULL;
}
Declaration::Declaration(Type * t, Tree<Area> * p, Function * f)
{
	this->cas = DEC_FUN;
	this->v_init = true;
	this->position = p;
	this->type = t;
	this->function = f;
}
//	public:
void Declaration::init(void)
{
	this->v_init = true;
}
bool Declaration::isinit(void)const
{
	return this->v_init;
}
Type * Declaration::getType(void)const
{
	return this->type;
}
Function * Declaration::getFun(void)const
{
	return this->function;
}
Tree<Area> * Declaration::getPos(void)const
{
	return this->position;
}
basedec Declaration::getDecType(void)const
{
	return this->cas;
}

void Declaration::setType(Type * t)
{
	this->type = t;
}

void Declaration::exportToXML(ostream & out)
{
	out << "<declaration id=\"" << this << "\" area=\"" << position->getData()->getID() << "\">" << endl;
	switch(cas)
	{
		case DEC_VAR: out << "<variable type=\""<< type << "\"/>" << endl;
			break;
		case DEC_FUN: if ( function != NULL ) function->exportToXML(out);
			break;
		case DEC_TYPE: if ( type != NULL ) type->exportToXML(out);
			break;
	}
	out << "</declaration>" << endl;
}
//};
