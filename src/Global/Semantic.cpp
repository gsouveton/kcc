#include "Semantic.hpp"

Semantic::Semantic()
{
	this->functions = new Tree<Function>();
	this->fptr = NULL;
	this->types = new Tree<Type>();
	this->tptr = NULL;
	this->currentDec = NULL;
	
	/*création des types de base*/
	Couple c;
	Type* t;
	Declaration* d;
	

	c = put("bool");
	t = new Type(TYPE_BOOL);
	d = new Declaration(t, position);
	addMeaning(c,d);
	
	c = put("int");
	t = new Type(TYPE_INT);
	d = new Declaration(t, position);
	addMeaning(c,d);
	
	c = put("float");
	t = new Type(TYPE_REAL);
	d = new Declaration(t, position);
	addMeaning(c,d);
	
	c = put("string");
	t = new Type(TYPE_STRING);
	d = new Declaration(t, position);
	addMeaning(c,d);
	
	c = put("char");
	t = new Type(TYPE_CHAR);
	d = new Declaration(t, position);
	addMeaning(c,d);
	
}
Semantic::~Semantic()
{
	this->functions->DeleteBranch();
	this->types->DeleteBranch();
}

void Semantic::dumpDeclaration()const
{
	Semantic * tmp = (Semantic*)this;
	tmp->ToXML(cout);
}

void Semantic::end()
{
	cout << "This should be the output of the compiler, if it reach this state, then we managed to read the whole code file" << endl;
}

void Semantic::newVariable(string nom_var ,string type_var )
{
	cout << "newvar : " << nom_var << "(" << type_var << ")" <<  endl;
	Type * t = getType(type_var); //on obtient le type à partir de son nom
	if ( t == NULL ) return error(UNKNOWN_TYPE);
	Couple c = put(nom_var); // on injecte, au besoin, le nom de la variable pour en extraire le code lexico
	if ( var_in_current_region(c) ) return error(IMPOSSIBLE_OVERLOAD); // on vérifie qu'une variable du même nom n'existe pas déjà dans la région courrante
	Declaration * d = new Declaration( t, this->position ,false); //on déclare la variable
	addMeaning(c, d); //on ajoute la déclaration dans les sens du mot
}

bool Semantic::var_in_current_region(Couple c)const
{
	Tree<Declaration> * m = meaning(c); // on récuperer tous les sens du mot indexé par c
	while ( m != NULL ) // tant qu'on dispose encore de sens
	{
		Declaration * d = m->getData(); // on récupere la déclarztion associée
		if ( d->getDecType() == DEC_VAR ) // on filtre les déclarations de variable
		{
			if (d->getPos()->getData()->getID() == position->getData()->getID() ) // si la variable en question est déclarée dans la région courrante
			{
				return true;
			}
		}
		m = m->getRight();
	}
	return false;
}

bool Semantic::type_in_current_region(Couple c)const //idem var_in_current_region
{
	Tree<Declaration> * m = meaning(c);
	while ( m != NULL )
	{
		Declaration * d = m->getData();
		if ( d->getDecType() == DEC_TYPE )
		{
			if (d->getPos()->getData()->getID() == position->getData()->getID() )
			{
				return true;
			}
		}
		m = m->getRight();
	}
	return false;
}

bool __isAbove(Tree<Area> * bas, Tree<Area> * haut) //simple fonction de vérification de parenté entre deux régions
{
	if (bas == haut) return true; /* condition d'arret de la récursivité */
	if ( bas == NULL) return false;
	return __isAbove(bas->getUp(), haut);
}


Type * Semantic::getType(string s)const //
{
	cout << "getType : " << s << endl;
	Type * lastfound = NULL;
	int area_id = -1;
		Tree<Declaration> * m = meaning(s);
		cout << "meanings of the word found" << endl;
		while ( m != NULL )
		{
			Declaration * d = m->getData();
			if ( d->getDecType() == DEC_TYPE )
			{
				Tree<Area> * dec_area = d->getPos();
				if ( __isAbove(position, dec_area) && area_id < dec_area->getData()->getID() )
				{
					lastfound = d->getType();
					area_id = dec_area->getData()->getID();
				}
			}
			m = m->getRight();
		}
		return lastfound;
}


void Semantic::newType(string nom_type)
{
	cout << "newtype : " << nom_type << endl;
	Couple c = put(nom_type);
	if (type_in_current_region(c)) return error(IMPOSSIBLE_OVERLOAD);
	Type * t = new Type();
	Declaration * d = new Declaration(t, position);	addMeaning(c, d);
	this->currentDec = d;
}

void Semantic::addField(string nom_du_champ ,string type_du_champ )//-> ajouter un champ dans une structure
{
	Couple cn = put(nom_du_champ);
	Type * t = getType(type_du_champ);
	if ( t == NULL ) return error(UNKNOWN_TYPE);
	this->currentDec->getType()->setStruct();
	this->currentDec->getType()->addField(cn,t);;
}

void Semantic::addDim(int i, int j)
{
	this->currentDec->getType()->addDim(i,j);
}

void Semantic::setType(string s)
{
	Type * t = getType(s);
	if ( t == NULL ) return error(UNKNOWN_TYPE);
	this->currentDec->getType()->setVect(t);
}


void Semantic::addFunction(string name,Tree<FUN_Args> * args, Tree<Instruction> * content, string return_name)
{
Tree<Area> * upper_area = position->getUp();
Couple c = put(name);
Type * t = getType(return_name);
if ( t == NULL ) throw("Error, no type");
Function * f = new Function(position, args, t);
position->getData()->getContent()->addSon(content);
 
}

void Semantic::addFunction(string name,Tree<FUN_Args> * args, Tree<Instruction> * content)
{
Tree<Area> * upper_area = position->getUp();
Couple c = put(name);
Function * f = new Function(position, args, NULL);
position->getData()->getContent()->addSon(content);
 
}

Tree<FUN_Args>* Semantic::addArg(string nom_arg ,string type_arg)
{
	Type * t = getType(type_arg);
	if ( t == NULL ) throw(UNKNOWN_TYPE);
	Couple c = put(nom_arg);
	newVariable(nom_arg, type_arg);
	return new Tree<FUN_Args>(new FUN_Args(c, t));
}

Tree<Expression> * Semantic::addParam(Tree<Expression> * p, Tree<FUN_Args> * a)
{
	Tree<Expression> * t = new Tree<Expression>((Expression*)a);
	p->addBrother(t);
	return p;
}

/*
void Semantic::endFunction()
{
	Function * f = currentDec->getFun();
	Tree<Declaration> * m = meaning(current_name);
	while ( m != NULL )
	{
		Declaration * d = m->getData();
		if ( d->getDecType() == DEC_FUN )
		{
			Function * g = d->getFun();
			if ( f->getNArgs() == g->getNArgs() && g != f && currentDec->getPos() == d->getPos() )
			{
				int i = f->getNArgs();
				bool oups = true;
				for ( int j = 1 ; j <= i ; j++ )
				{
					if (f->getArgType(j) != g->getArgType(j)) oups = false;
				}
				if ( oups ) error(IMPOSSIBLE_OVERLOAD);
			}
		}
	}
	stepBack();
}*/

Declaration * Semantic::getVar(string s)const
{
	Declaration * lastfound = NULL;
	int area_id = -1;
		Tree<Declaration> * m = meaning(s);
		while ( m != NULL )
		{
			Declaration * d = m->getData();
			if ( d->getDecType() == DEC_VAR )
			{
				Tree<Area> * dec_area = d->getPos();
				if ( __isAbove(position, dec_area) && area_id < dec_area->getData()->getID() )
				{
					lastfound = d;
					area_id = dec_area->getData()->getID();
				}
			}
		}
		return lastfound;
}


Type* Semantic::accessVar(string var)//-> Vérifie si la variable de nom var existe et est accessible
{
	Declaration* d = getVar(var);
	if ( d == NULL )
	{
		error(NO_VAR);
		return NULL;
	}
	tptr = d->getType();
	
	return tptr;
}

Type* Semantic::inVect(int taille)
{
	if ( tptr->getDim() == taille )
	{
		tptr = tptr->getType();
		return tptr;
	}
	else
	{
		error(WRONG_DIM);
		tptr = NULL;
		return NULL;
	}
}

Type* Semantic::inStruct(string champ)
{
	Couple c = put(champ);
	tptr = tptr->getFieldType(c); // Attention peut etre plantage si type est un vect //
	if (tptr == NULL) error(WRONG_FIELD);
	return tptr;
}

void Semantic::closeVar(void){}

Tree<Expression> * Semantic::buildExpr(Tree<Expression>  * l , Tree<Expression>  * r )
{
	l->addBrotherhood(r);
	return l;
}




void Semantic::error(sem_error errorlvl)const
{
	switch(errorlvl)
	{
		case UNKNOWN_TYPE: cerr << "Type dosn't exist" << endl;
			break;
		case IMPOSSIBLE_OVERLOAD: cerr << "Variable already exist in current area, unable to overload" << endl;
			break;
		case DUPLICATE_ARG:cerr << "An arg with the same name is defined before" << endl;
			break;
		case NO_VAR:cerr << "Variable doesn't exist" << endl;
			break;
		case WRONG_DIM:cerr << "Vect type used with wrong dimension" << endl;
			break;
		case WRONG_FIELD:cerr << "Struct type used with wrong field" << endl;
			break;
			
		default: cerr << "Quantum error : Unable to compile for some ungiven reason";
	}
}


// Fonction de calcul sur les types de base.
Tree<Expression> * Semantic::calc(Tree<Expression>* l ,Tree<Expression>* r, operand o)
{
	basetype lt, rt, result_t;

	if ( ! ( l->getData()->getType()->isBaseType() ) )
		throw("Error : l_value isn't a basetype");
	if ( ! ( r->getData()->getType()->isBaseType() ) )
		throw("Error : r_value isn't a basetype");

	lt = l->getData()->getType()->baseType();
	rt = r->getData()->getType()->baseType();

	if( lt == rt)
		result_t = lt;
	else if ( lt == TYPE_INT && rt == TYPE_REAL )
		result_t = rt;
	else if ( lt == TYPE_REAL && rt == TYPE_INT )
		result_t = lt;
	else
		throw("Error : incompatible types in addition");
		
	if ( result_t == TYPE_CHAR )
		throw("Error : cannot add two chars");
	else if ( result_t == TYPE_STRING && o != PLUS )
		throw("Error : operator o is not overloaded for string type");
	else if ( result_t == TYPE_BOOL && o != PLUS && o != TIME )
		throw("Error : operator o is not overloaded for boolean type");
	
	Type* result = new Type(result_t);
	Expression* result_e = new Expression(CALC, result, l ,r , o);
	Tree<Expression> * t = new Tree<Expression>(result_e);

	return t;
}

// Comparaison sur des types de base uniquement
Tree<Expression> * Semantic::comparaison(Tree<Expression> * l, Tree<Expression> * r , rel relation)
{
	basetype lt, rt, result_t;

	if ( ! ( l->getData()->getType()->isBaseType() ) )
		throw("Error : left member of comparison isn't a basetype");
	if ( ! ( r->getData()->getType()->isBaseType() ) )
		throw("Error : right member of comparison isn't a basetype");

	lt = l->getData()->getType()->baseType();
	rt = r->getData()->getType()->baseType();

	if( lt == rt)
		result_t = lt;
	else if ( lt == TYPE_INT && rt == TYPE_REAL )
		result_t = rt;
	else if ( lt == TYPE_REAL && rt == TYPE_INT )
		result_t = lt;
	else
		throw("Error : incompatible types in comparaison");
		
	if ( result_t == TYPE_STRING )
		throw("Error : cannot compare two strings");
	else if ( result_t == TYPE_BOOL )
		throw("Error : cannot compare two booleans");

	Expression* comparison = new Expression(COMP, l ,r , relation);
	Tree<Expression> * t = new Tree<Expression>(comparison);

	return t;
}

// Création d'une constante (valeur)
Tree<Expression> * Semantic::newConst(char* val, basetype b)
{
	Expression* constante = new Expression(CONST, b, val);
	Tree<Expression> * t = new Tree<Expression>(constante);
	return t;
}

Tree<Expression> * Semantic::callFunction(string s, Tree<Expression> * args)
{
	Function * f = getFunction(s,args);
	if ( f == NULL )
		throw("Error : undefined function f");
	Expression* func = new Expression(CALL, f, args);
	Tree<Expression> * t = new Tree<Expression>(func);
	return t;
}


Function * Semantic::getFunction(string s, Tree<Expression> * t)
{
	Function * match = NULL;
	int area_id = -1;
	Tree<Declaration> * m = meaning(s);
	
	Tree<Expression> * ptr = t;
	int nargs = 0;
	while ( t != NULL )
	{
			nargs++;
			ptr = ptr->getRight();
	}
	while ( m != NULL )
	{
		Declaration * d = m->getData();
		Tree<Area> * dec_area = d->getPos();
		if ( d->getDecType() == DEC_FUN && __isAbove(position, dec_area) && area_id < dec_area->getData()->getID() )
		{
			Function * f = d->getFun();
				if ( f->getNArgs() == nargs)
				{
					bool sofar = true;
					ptr = t;
					for ( int i = 1 ; i <= nargs ; i++ )
					{
						Type * t = f->getArgType(i);
						if ( t != ptr->getData()->getType() ) sofar = false;
						ptr = ptr->getRight();
					}
					if ( sofar )
					{
						area_id = d->getPos()->getData()->getID();
						match = f;
					}
				}
		}
	}
	return match;
}


Tree<Expression> * Semantic::voidExpr(void)
{
	return NULL;
}

Tree<Instruction> * Semantic::voidIns(void)
{
	return NULL;
}

Tree<Variable> * Semantic::emptyListVar(void)
{
	return NULL;
}

Tree<Variable> * Semantic::listVar(Variable * v)
{	// On ne vérifie pas le pointeur Var* suppose correct
	Tree<Variable> * t = new Tree<Variable>(v);
	return t;
}

Tree<Variable> * Semantic::addListVar(Tree<Variable> * t, Variable * v)
{	// On suppose t non null
	t->addBrother(v);
	return t;
}

Tree<Expression> * Semantic::listExpr(Expression * e)
{	// On ne vérifie pas le pointeur Expr* suppose correct
	Tree<Expression> * t = new Tree<Expression>(e);
	return t;
}

Tree<Expression> * Semantic::addListExpr(Tree<Expression> * t, Expression * e)
{	// On suppose t non null
	t->addBrother(e);
	return t;
}


Variable * Semantic::newVar(string idf)
{
	Declaration* d = getVar(idf);
	if(d == NULL)
		throw("Undefined var : idf");
	
	Variable* v = new Variable(d);
	return v;
}

Variable * Semantic::newVar(string idf, Tree<Accesseur> * a)
{
	Declaration* d = getVar(idf);
	if(d == NULL)
		throw("Undefined var : idf");
	
	
	Type * t = d->getType();
	Tree<Accesseur> * ptr = a;
	
	while ( ptr != NULL and t != NULL )
	{
		Accesseur * acc = ptr->getData();
		accesseur type_acces = acc->a;
				if ( type_acces == VECT )
				{
					Tree<Expression> * te = acc->t;
					int card = te->cardinal();
					if ( t->getDim() == card )
					{
						bool oups = false;
						while ( te != NULL )
						{
							if ( ! (te->getData()->getType()->isBaseType() && te->getData()->getType()->baseType() == TYPE_INT) ) oups = true;
							te = te->getRight();
						}
						if ( ! oups ) t = t->getType(); else t = NULL;
					}
					else t = NULL;
				}
				else if ( type_acces == STRUCT )
				{
					t = t->getFieldType(acc->c);
				}
		ptr = ptr->getRight();
	}
	
	Variable* v = new Variable(d,t,a);
	return v;
}

Tree<Accesseur>  * Semantic::newAcc(Tree<Expression> * te ,Tree<Accesseur> *ta)
{
	Accesseur * A = new Accesseur();
	A->t = te;
	Tree<Accesseur> * T = new Tree<Accesseur>(A);
	T->addBrotherhood(ta);
	return T;	
}

Tree<Accesseur>  * Semantic::newAcc(Tree<Expression> * te)
{
	Accesseur * A = new Accesseur();
	A->t = te;
	return new Tree<Accesseur>(A);
}
Tree<Accesseur>  * Semantic::newAcc(string s)
{
	Accesseur * A = new Accesseur();
	Couple c = put(s);
	A->c = c;
	return new Tree<Accesseur>(A);
}
Tree<Accesseur>  * Semantic::newAcc(string s,Tree<Accesseur> * ta)
{
	Accesseur * A = new Accesseur();
	Couple c = put(s);
	A->c = c;
	Tree<Accesseur> * T = new Tree<Accesseur>(A);
	T->addBrotherhood(ta);
	return T;
}

// Affectation
Tree<Instruction> * Semantic::affectation(Variable * var, Tree<Expression> * r , opaff oa)
{
	basetype lt, rt, result_t;

	if ( ! ( var->getType()->isBaseType() ) )
		throw("Error : l_value isn't a basetype");
	if ( ! ( r->getData()->getType()->isBaseType() ) )
		throw("Error : r_value isn't a basetype");

	lt = var->getType()->baseType();
	rt = r->getData()->getType()->baseType();

	if( lt == rt)
		result_t = lt;
	else if ( lt == TYPE_INT && rt == TYPE_REAL )
		result_t = rt;
	else if ( lt == TYPE_REAL && rt == TYPE_INT )
		result_t = lt;
	else
		throw("Error : incompatible types in assignment");

	Instruction * affec = new Instruction(INS_AFF , var , r , oa);
	Tree<Instruction> * t = new Tree<Instruction>(affec);

	return t;
}

Tree<Instruction> * Semantic::write(string text, Tree<Expression> * args)
{
	int i = 1;
	size_t pos = 0;
	char nextchar;
	char* nextchars;
	string tmp = text, s_nextchars;
	Tree<Expression> * tmp_args = args;
	Type * tmp_type = NULL;
	basetype bt;

	// On cherche les occurences de "\%" et on les remplace.
	while( ( pos = text.find ( "\%",pos ) ) != -1 )	// On cherche dans text qui n'est pas modifié.
		tmp.replace ( pos , 1 , "" , 0 ) ;				// On cherche à partir du dernier endroit ou on a trouvé une occurence.
	while( ( pos = tmp.find ( "%" , pos ) ) != -1 )
	{
		i++;
		s_nextchars = tmp.substr(pos+1,1);
		nextchars = const_cast<char*>(s_nextchars.c_str());
		nextchar = nextchars[0];
		if(tmp_args == NULL)
			throw("Error : missing 1 argument to function write, matching %nextchar");
		tmp_type = tmp_args -> getData() -> getType();
		if( ! (tmp_type -> isBaseType () ) )
			throw("Error : cannot displays a value for the non-basetyped argument i");
		bt = tmp_type-> baseType();
		switch(nextchar)
		{
			case 'd': if ( bt != TYPE_INT ) throw ("Error : argument i passed to write() must be an INTEGER"); break;
			case 'f': if ( bt != TYPE_REAL ) throw ("Error : argument i passed to write() must be an REAL"); break;
			case 'r': if ( bt != TYPE_REAL ) throw ("Error : argument i passed to write() must be an REAL"); break;
			case 'b': if ( bt != TYPE_BOOL ) throw ("Error : argument i passed to write() must be an BOOL"); break;
			case 'c': if ( bt != TYPE_CHAR ) throw ("Error : argument i passed to write() must be an CHAR"); break;
			case 's': if ( bt != TYPE_STRING ) throw ("Error : argument i passed to write() must be an STRING"); break;
			default: throw("Error : %nextchar is not a valid poursangtay");
		}
		tmp_args = tmp_args->getRight();
	}
	// Maintenant on vérifie qu'il n'y a pas d'arguments en trop
	i = 0;
	while(tmp_args != NULL)
	{
		i++;
		tmp_args = tmp_args->getRight();
	}
	if(i > 0)
		throw("You're a kitty ! You called write with i unmatched parameters");
	// Ow, les tests sont finis on crée un objet :)
	Instruction * w = new Instruction(INS_WRITE,text,args);
	Tree<Instruction> * t = new Tree<Instruction>(w);
	return t;
}


Tree<Instruction> * Semantic::read(string text, Tree<Expression> * args)
{
	int i = 1;
	size_t pos = 0;
	char nextchar;
	char* nextchars;
	string tmp = text, s_nextchars;
	Tree<Expression> * tmp_args = args;
	Type * tmp_type = NULL;
	basetype bt;

	// On cherche les occurences de "\%" et on les remplace.
	while( ( pos = text.find ( "\%",pos ) ) != -1 )	// On cherche dans text qui n'est pas modifié.
		tmp.replace ( pos , 1 , "" , 0 ) ;				// On cherche à partir du dernier endroit ou on a trouvé une occurence.
	while( ( pos = tmp.find ( "%" , pos ) ) != -1 )
	{
		i++;
		s_nextchars = tmp.substr(pos+1,1);
		nextchars = const_cast<char*>(s_nextchars.c_str());
		nextchar = nextchars[0];
		if(tmp_args == NULL)
			throw("Error : missing 1 argument to function read, matching %nextchar");
		tmp_type = tmp_args -> getData() -> getType();
		if( ! (tmp_type -> isBaseType () ) )
			throw("Error : cannot store any value for the non-basetyped argument i");
		bt = tmp_type-> baseType();
		switch(nextchar)
		{
			case 'd': if ( bt != TYPE_INT ) throw ("Error : argument i passed to read() must be an INTEGER"); break;
			case 'f': if ( bt != TYPE_REAL ) throw ("Error : argument i passed to read() must be an REAL"); break;
			case 'r': if ( bt != TYPE_REAL ) throw ("Error : argument i passed to read() must be an REAL"); break;
			case 'b': if ( bt != TYPE_BOOL ) throw ("Error : argument i passed to read() must be an BOOL"); break;
			case 'c': if ( bt != TYPE_CHAR ) throw ("Error : argument i passed to read() must be an CHAR"); break;
			case 's': if ( bt != TYPE_STRING ) throw ("Error : argument i passed to read() must be an STRING"); break;
			default: throw("Error : %nextchar is not a valid poursangtay");
		}
		tmp_args = tmp_args->getRight();
	}
	// Maintenant on vérifie qu'il n'y a pas d'arguments en trop
	i = 0;
	while(tmp_args != NULL)
	{
		i++;
		tmp_args = tmp_args->getRight();
	}
	if(i > 0)
		throw("You're a kitty ! You called read with i unmatched parameters");
	// Ow, les tests sont finis on crée un objet :)
	Instruction * w = new Instruction(INS_READ,text,args);
	Tree<Instruction> * t = new Tree<Instruction>(w);
	return t;
}





// Gestion de l'incrémentation / décrémentation
Tree<Expression> * Semantic::increment(Tree<Expression> * te , PosExpr pe )
{
	if ( te == NULL )
		throw("Error : wrong usage of operator ++");

	// Well, there is only one class for all unaries(Expr)->Expr functions.
	



}

Tree<Expression> * Semantic::decrement(Tree<Expression> * te , PosExpr pe )
{
	if ( te == NULL )
		throw("Error : wrong usage of operator --");

}

Tree<Expression> * Semantic::notExpr(Tree<Expression> * te , PosExpr pe )
{
	if ( te == NULL )
		throw("Error : impossible to use negation operator on an empty expression");

}

Tree<Expression> * Semantic::diffExpr(Tree<Expression> * te , PosExpr pe )
{
	if ( te == NULL )
		throw("Cannot use unary - operator on an empty expression");
}		
		
		
/////////////////////////////////
// -> Useless functions.

void Semantic::beginDeclarations(){};//-> Nouveau bloc de déclarations (xml : <declarations>)
void Semantic::endDeclarations(){};//-> Fin bloc de déclarations (xml : </declarations>)
void Semantic::beginInstructions(){};//-> Nouveau bloc d'instrcutions (xml : <instructions>)
void Semantic::endInstructions(){};//-> Fin bloc d'instrcutions (xml : </instructions>)*/

//
/////////////////////////////////