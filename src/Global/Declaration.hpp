#ifndef DECLARATION_HPP
#define DECLARATION_HPP

#include "Global.hpp"
#include "Type.hpp"
#include "Area.hpp"
#include "Tree.hpp"
#include "Function.hpp"


class Declaration
{
	protected:
		basedec cas;
		bool v_init;
		Tree<Area> * position;
		Type* type;
		Function* function;

	public:
		Declaration(Type *, Tree<Area> *, bool); //var
		Declaration(Type *, Tree<Area> *); //type
		Declaration(Type *, Tree<Area> *,  Function *); //function
	public:
		void init(void);
		bool isinit(void)const;
		Type * getType(void)const;
		Function * getFun(void)const;
		Tree<Area> * getPos(void)const;
		basedec getDecType(void)const;
		void setType(Type *);
		void exportToXML(ostream & out);
};
#endif
