#include "Expression.hpp"





Variable::Variable(Declaration* d)
{
	var         =  d;
	type	    =  d->getType();
	acc  		=  NULL;
}

Variable::Variable(Declaration* d, Type * t, Tree<Accesseur> * a)
{
	var         =  d;
	type	    =  t;
	acc  		=  a;
}

Type* Variable::getType()
{
	return this->type;
}
void Variable::exportToXML(ostream & out) const
{

}

Calc::Calc(Tree<Expression> * l, Tree<Expression> * r,operand op)
{
	left = l;
	right = r;
	o = op;
}

void Calc::exportToXML(ostream & out) const
{
	out << "<operation type=\"" << o << "\">" << endl << "<left>" << endl;
	left-> exportToXML(out);
	out << "<left>" << endl << "<right>" << endl;
	right-> exportToXML(out);
	out << "</right>" << endl << "</operation>" << endl;
}


Comparaison::Comparaison(Tree<Expression> * l, Tree<Expression> * r, rel re)
{
	left = l;
	right = r;
	rela = re;
}

void Comparaison::exportToXML(ostream & out) const
{
	out << "<compare type=\"" << rela << "\">" << endl << "<left>" << endl;
	left-> exportToXML(out);
	out << "<left>" << endl << "<right>" << endl;
	right-> exportToXML(out);
	out << "</right>" << endl << "</compare>" << endl;
}


Call::Call(Function * f , Tree<Expression> * a)
{
	func = f;
	args = a;
}

void Call::exportToXML(ostream & out) const
{

}

void OpExpr::exportToXML(ostream & out) const
{

}

void Constante::exportToXML(ostream & out) const
{

}

Expression::Expression(TypeExpr te, Type* t, Tree<Expression> * l , Tree<Expression> * r ,operand o)
{
	type = t;
	var    = NULL; constante  = NULL; call  = NULL;	
	comp   = NULL; calc       = NULL; oexpr = NULL;
	if(te == CALC)
		this->calc = new Calc(l,r,o);
}

Expression::Expression(TypeExpr te, Tree<Expression> * l , Tree<Expression> * r , rel relation)
{
	Type * t = new Type(TYPE_BOOL);
	type = t;
	var    = NULL; constante  = NULL; call  = NULL;	
	comp   = NULL; calc       = NULL; oexpr = NULL;
	if(te == COMP)
		this->comp = new Comparaison(l,r,relation);
}

Expression::Expression(TypeExpr te, basetype b, char* val)
{
	Type * t = new Type(b);
	type = t;
	var    = NULL; constante  = NULL; call  = NULL;	
	comp   = NULL; calc       = NULL; oexpr = NULL;
	if(te == CONST)
		this->constante = new Constante(b, val);
}


Expression::Expression(TypeExpr te, Function * f, Tree<Expression> * args)
{
	Type * t = f->getReturn();
	type = t;
	var    = NULL; constante  = NULL; call  = NULL;	
	comp   = NULL; calc       = NULL; oexpr = NULL;
	if(te == CALL)
		this->call = new Call(f, args);
}

Type* Expression::getType()
{
	return type;
}

void Expression::exportToXML(ostream & out) const
{
	if(e == VARIABLE)        var->exportToXML(out);
	else if(e == CONST)      constante->exportToXML(out);
	else if(e == CALL)       call->exportToXML(out);
	else if(e == COMP)       comp->exportToXML(out);
	else if(e == CALC)       calc->exportToXML(out);
	else if(e == OP_EXPR)    oexpr->exportToXML(out);
}
