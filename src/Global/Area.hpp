#ifndef AREA_HPP
#define AREA_HPP
#include "Global.hpp"
#include "Instruction.hpp"
#include "Tree.hpp"

class Area
{
	protected:
		int id;
		Tree<Instruction> * content;
	public:
		Area(int);
		~Area(void);
	public:
		int getID()const;
		Tree<Instruction> * getContent(void)const;
		void exportToXML(ostream & out)const;
};

class Area_Map
{
	protected:
		Tree<Area> * map;
		int id;
		Tree<Area> * position;
	public:
		Area_Map();
		~Area_Map();
	public:
		void addArea();
		void stepBack();
		Tree<Area> * getArea(int n)const;
		Tree<Area> * getPosition(void)const;
};

#endif