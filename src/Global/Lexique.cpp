#include "Lexique.hpp"

//class Word
//{
//	private:
//		string word;
//		int l;
//		int id;
//
//	public:
Word::Word(string s, int i)
{
	this->word = s;
	this->id = i;
	this->l = s.length();
	this->meaning = new Tree<Declaration>();
}
Word::~Word(void)
{
	this->meaning->DeleteBranch();
}
//	public:
int Word::getID(void)const
{
	return this->id;
}
string Word::getWord(void)const
{
	return this->word;
}
int Word::getLength(void)const
{
	return this->l;
}
Tree<Declaration> * Word::getMeaning(void)const
{
	cout << "word address is : " << this << endl;
	if (this->meaning != NULL) return this->meaning->getDown();
		else return NULL;
}
void Word::addMeaning(Declaration* d)
{
	this->meaning->addSon(d);
}

void Word::exportToXML(ostream & out)
{
	out << "<word id=\"" << id << "\" value=\""<< word << "\">" << endl;
	meaning->exportToXML(out);
	out << "</word>" << endl;
}
//};



//class Lex_table
//{
//	private:
//		Tree<Word> * table;
//		int id;
//
//	public:
Lex_table::Lex_table()
{
	this->table = new Tree<Word>();
	this->id = 0;

	for ( int i = 0 ; i < 211 ; ++i )
	{
		this->table->addSon(new Tree<Word>());
	}
}
Lex_table::~Lex_table()
{
	this->table->DeleteBranch();
}
//
//	public:
Couple Lex_table::put(string s)
{
	int h = hash(s);
	Couple a = _get(s, h);
	if ( a == Couple(0,0) ) return _put(s, h);
	else return a;
}
string Lex_table::get(Couple a) const
{
	return this->get(a.x, a.y);
}
string Lex_table::get(int x, int y) const
{
	Tree<Word> * ptr = this->table->getDown();
	while ( ptr != NULL && x > 0)
	{
		--x;
		ptr = ptr->getRight();
	}
	ptr = ptr->getDown();
	while ( ptr != NULL )
	{
		Word * w = ptr->getData();
		if ( w->getID() == y ) return w->getWord();
		ptr = ptr->getRight();
	}
	return "";
};

Tree<Declaration> * Lex_table::meaning(string s)const
{
	return this->_getW(s)->getMeaning();
}
Tree<Declaration> * Lex_table::meaning(int x, int y)const
{
	Word * w = _getW(x,y);
	if ( w != NULL )	return w->getMeaning();
		else			return NULL;
}
Tree<Declaration> * Lex_table::meaning(Couple c )const
{
	return this->_getW(c)->getMeaning();
}


//	private:
int Lex_table::hash(string s)const
{
	char* p;
                char* c = const_cast<char*>(s.c_str());
                unsigned h = 0, g;

                for(p = c; *p != '\0' ; p++)
                {
                        h = (h << 4) + (*p);
                        if((g = h & 0xF0000000) != 0)
                        {
                                h = h ^ (g >> 24);
                                h = h ^ g;
                        }
                }
                return h % 211;
}
Couple Lex_table::get(string s)const
{
	return _get(s, hash(s));
}

void Lex_table::addMeaning(Couple c, Declaration * d)
{
	Word * w = _getW(c);
	w->addMeaning(d);
}


Couple Lex_table::_put(string s , int i)
{
	Tree<Word> * ptr = this->table->getDown();
	int j = i;
	while ( ptr != NULL && j > 0 )
	{
	--j;
		ptr = ptr->getRight();
	}
	int a = ++id;
	ptr->addSon(new Tree<Word>(new Word(s, a)));
	return Couple(i, a);
}
Couple Lex_table::_get(string s , int i)const
{
	Tree<Word> * ptr = this->table->getDown();
	int j = i;
	while ( ptr != NULL && j > 0 )
	{
		--j;
		ptr = ptr->getRight();
	}
	ptr = ptr->getDown();
	while ( ptr != NULL )
	{
		Word * w = ptr->getData();
		if ( w->getWord() == s ) return Couple(i, w->getID() );
		ptr = ptr->getRight();
	}
	return Couple(0,0);
}

Word * Lex_table::_getW(string s)const
{
	int h = hash(s);
	Tree<Word> * ptr = this->table->getDown();
	while ( ptr != NULL && h > 0 )
	{
		--h;
		ptr = ptr->getRight();
	}
	ptr = ptr->getDown();
	while ( ptr != NULL )
	{
		Word * w = ptr->getData();
		if ( w->getWord() == s ) return w;
		ptr = ptr->getRight();
	}
	return NULL;
}
Word * Lex_table::_getW(int x , int y )const
{
	int h = x;
	Tree<Word> * ptr = this->table->getDown();
	while ( ptr != NULL && h > 0 )
	{
		--h;
		ptr = ptr->getRight();
	}
	ptr = ptr->getDown();
	while ( ptr != NULL )
	{
		Word * w = ptr->getData();
		if ( w->getID() == y ) return w;
		ptr = ptr->getRight();
	}
	return NULL;
}
Word * Lex_table::_getW(Couple c)const
{
	return _getW(c.x, c.y);
}

void Lex_table::ToXML(ostream & out)
{
	out << "<LexTable>" << endl;
	Tree<Word> * ptr = this->table->getDown();
	int i = 0;
	while ( ptr != NULL )
	{
		out << "<lex_slice x=\"" << i << "\">" << endl;
		if ( ptr->getDown() != NULL ) ptr->getDown()->exportToXML(out);
		ptr=ptr->getRight();
		i++;
		out << "</lex_slice>" << endl;
	}
	out << "</LexTable>"<< endl;
}


//};

/*
int main(void)
{
	Lex_table L; 
	Area_Map M;
	Type * INT = new Type(TYPE_INT); 
	Type * VECT_INT = new Type(); 
	VECT_INT->setVect(INT); 
	VECT_INT->addDim(0,10); 
	VECT_INT->addDim(0,10); 
	VECT_INT->addDim(0,10); 
	Type * STRUCT_INT = new Type(); 
	STRUCT_INT->setStruct();
	STRUCT_INT->addField(Couple(10,10), INT); 
	Declaration * d1 = new Declaration(INT, M.getPosition());
	Declaration * d2 = new Declaration(VECT_INT, M.getPosition());
	Declaration * d3 = new Declaration(STRUCT_INT, M.getPosition());

	L.addMeaning(L.put("int"),d1);
	L.addMeaning(L.put("int_v"),d2);
	L.addMeaning(L.put("int_vs"),d3);



	L.ToXML(cout);
	return 0;
}*/
