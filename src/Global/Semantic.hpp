#ifndef SEMANTIC_HPP
#define SEMANTIC_HPP

#define analyser

#include "Global.hpp"
#include "Tree.hpp"
#include "Area.hpp"
#include "Declaration.hpp"
#include "Type.hpp"
#include "Function.hpp"
#include "Lexique.hpp"
#include "Expression.hpp"
#include "Instruction.hpp"

class Semantic : public Lex_table, public Area_Map
{
	typedef enum { UNKNOWN_TYPE , IMPOSSIBLE_OVERLOAD, DUPLICATE_ARG,  NO_VAR, WRONG_DIM, WRONG_FIELD } sem_error;

	protected:
		Tree<Function> * functions, fptr;
		Tree<Type> * types;
		Type* tptr;
		Declaration * currentDec;
		string current_name;
		public:
		Semantic();
		~Semantic();


	public:
	
		void dumpDeclaration(void) const;
		Type * getType(string)const;
		void newVariable(string nom_var ,string type_var );//-> nouvelle variable : OK
		void newType(string nom_type );//-> créer un nouveau type :  OK
		void addField(string nom_du_champ ,string type_du_champ ); //-> ajouter un champ dans une structure : OK
		void addDim(int, int); // ok
		void setType(string); //ok
		void addFunction(string,Tree<FUN_Args>*, Tree<Instruction> *, string);
		void addFunction(string,Tree<FUN_Args>*, Tree<Instruction> *);
		//void newFunction(string nom_function );// OK -> créer une nouvelle fonction
		//void addReturnType(string type_retour );// OK -> ajouter un type de retour à la fonction en cours de déclaration
		Tree<FUN_Args> * addArg(string nom_arg ,string type_arg );// OK-> ajouter un parametre a la fonction en cours de déclaration
		//void endFunction();// OK -> fin de la fonction actuellement en cours de déclaration (xml : </function>)
		void beginDeclarations();//-> Nouveau bloc de déclarations (xml : <declarations>)
		void endDeclarations();//-> Fin bloc de déclarations (xml : </declarations>)
		void beginInstructions();//-> Nouveau bloc d'instrcutions (xml : <instructions>)
		void endInstructions();//-> Fin bloc d'instrcutions (xml : </instructions>)*/
		
		Type * inStruct(string nom_champ );//-> Vérifie si un champ est bien membre de la structure en cours de manipulation
		Type * inVect(int taille );//-> Vérifie si la taille est conforme aux dimensions du tableau en cours de manipulation
		Type * accessVar(string var );//-> Vérifie si la variable de nom var existe et est accessible
		void closeVar(void);
		
		Type * returnType(string nom_fonction );//-> Retourne le type de la fonction nom_fonction
		bool compTypes(Type* type1 ,Type* type2 );//-> Vérifie si les types sont compatibles

		Tree<Expression> * calc        ( Tree<Expression> * , Tree<Expression> * , operand);
		Tree<Expression> * comparaison ( Tree<Expression> * , Tree<Expression> * , rel);
		
		Tree<Instruction> * affectation ( Variable * , Tree<Expression> * , opaff);
		
		void begin(void);
		void end(void);
		
		void inc(Tree<Expression> *);
		void dec(Tree<Expression> *);
		
	
		Tree<Expression> * newConst(char* val, basetype b);
		
		Tree<Expression>  * callFunction(string, Tree<Expression>*);
		Tree<Expression>  * voidExpr(void);			// Returns empty Tree<Expr> == NULL
		Tree<Instruction> * voidIns(void);			// Returns empty Tree<Ins> == NULL
		Tree<Variable>    * emptyListVar(void);		// Returns empty Tree<Var> == NULL <--- useless a priori
		Tree<Variable>    * listVar(Variable *);
		Tree<Variable>    * addListVar(Tree<Variable> * , Variable *);
		Tree<Expression>  * listExpr(Expression *);
		Tree<Expression>  * addListExpr(Tree<Expression> * , Expression *);
		Tree<Expression>  * buildExpr(Tree<Expression>  * , Tree<Expression>  * );
		Tree<Expression>  * functionReturn(Tree<Expression> *);	//-> La fonction "return" des fonctions
		Tree<Instruction> * write(string, Tree<Expression> *);
		Tree<Instruction> * read(string, Tree<Expression> *);
		Tree<Expression>  * priority(Tree<Expression> *); //-> Défini une priorité plus importante pour cette expression
																 //-> Crée un fils a l'expression courante et la place dedans
																 //-> puis retourne son père, pour que les expressions suivantes
																 //-> soient bien les frères de l'expression de base.

		Tree<Accesseur>  * newAcc(Tree<Expression> *,Tree<Accesseur> *); // -> Nouvel accesseur
		Tree<Accesseur>  * newAcc(Tree<Expression> *); // -> Nouvel accesseur
		Tree<Accesseur>  * newAcc(string); // -> Nouvel accesseur
		Tree<Accesseur>  * newAcc(string,Tree<Accesseur> *); // -> Nouvel accesseur
		
		Variable * newVar(string);
		Variable * newVar(string, Tree<Accesseur> *);
		Function * getFunction(string, Tree<Expression>*);
		
		Tree<Expression> * increment(Tree<Expression> * , PosExpr );
		
		Tree<Expression> * decrement(Tree<Expression> * , PosExpr );
		
		Tree<Expression> * notExpr(Tree<Expression> * , PosExpr );
		// The previous function can be only used on boolean-typed expressions.
		Tree<Expression> * diffExpr(Tree<Expression> * , PosExpr );
		// Please notice this last function can be used only on Numeric types.
		
		
		// Ajouter un parametre lors de l'appel d'une fonction
		Tree<Expression> * addParam(Tree<Expression> * , Tree<FUN_Args>*);
		
	protected:
		
		Declaration * getVar(string) const;
		bool var_in_current_region(Couple) const;
		bool type_in_current_region(Couple) const;
		

	protected: //toutes les fonctions de test sur les déclarations de fonction
		bool isOverLoadLegit(Function*)const;
		bool isOverLoadLegit(void)const;
	protected: // toutes les fonctions de test sur les déclarations de typedef
		bool hasField(Type*, string)const;
		bool hasField(Type*, Couple)const;


	protected:
		void error(sem_error)const;
};
#endif
